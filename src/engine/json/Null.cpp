#include "Null.hpp"

#include "../exception/InvalidArgumentException.hpp"



namespace tge {

namespace json {

nlohmann::json Null::toNlohmann() {
    return nlohmann::json(nullptr);
}

std::shared_ptr<Null> Null::fromNlohmann(const nlohmann::json& j) {
    if (j.is_null()) {
        return std::make_shared<Null>();
    } else {
        throw InvalidArgumentException("j", "nlohmann::json value is not null", EXCTX);
    }
}



Null::Null() : Value(Node::Type::_NULL) {}



std::string Null::toString() const {
    return "null";
}

}

}
