#pragma once

#include <memory>
#include <optional>

#include "lib/nlohmann/json.hpp"

#include "Value.hpp"



namespace tge {

namespace json {

// json::Uint is an unsigned whole number, like 0, 23, 453, 9000000000
class Uint : public Value {

public:
    unsigned long value;
    std::optional<unsigned long> getUlong() const;
    std::optional<long> getLong() const;

    nlohmann::json toNlohmann();
    static std::shared_ptr<Uint> fromNlohmann(const nlohmann::json& j);

    Uint(unsigned long value = 0);

    std::string toString() const;

};

}

}
