#include "Float.hpp"



namespace tge {

namespace json {

std::optional<double> Float::getDouble() const {
    return std::optional(value);
}



nlohmann::json Float::toNlohmann() {
    return nlohmann::json(value);
}

std::shared_ptr<Float> Float::fromNlohmann(const nlohmann::json& j) {
    std::shared_ptr<Float> result = std::make_shared<Float>();
    result->value = j.get<double>();
    return result;
}



Float::Float(double value) : Value(Node::Type::FLOAT) {
    this->value = value;
}



std::string Float::toString() const {
    return std::to_string(value);
}

}

}
