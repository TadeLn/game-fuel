#pragma once

#include <optional>

#include "lib/nlohmann/json.hpp"

#include "../Stringable.hpp"
#include "../exception/InvalidTypeException.hpp"
#include "../exception/InvalidCastException.hpp"



namespace tge {

namespace json {

class Node;

typedef std::vector<std::shared_ptr<Node>> array;
typedef std::map<std::string, std::shared_ptr<Node>> dictionary;

// json::Node can be a value (like 3, 2, true, or "string"), an array (like [1, 3, 5]) or an object (like {"a": "b", "c": false})
class Node : public Stringable {


public:
    enum class Type {
        UNDEFINED = -1,
        _NULL = 0,
        INT,
        UINT,
        FLOAT,
        STRING,
        BOOL,
        ARRAY,
        OBJECT
    };

    Type getType();



    // Create a new json::Null
    static std::shared_ptr<Node> from();

    // Create a new json::Node from a value
    template<typename T>
    static std::shared_ptr<Node> from(T value) {
        throw InvalidCastException(EXCTX);
    }

    // Create a new json::Node from a vector of values
    template<typename T>
    static std::shared_ptr<Node> from(const std::vector<T>& values) {
        std::vector<std::shared_ptr<Node>> nodes;
        nodes.reserve(values.size());
        for (auto it : values) {
            nodes.push_back(Node::from(it));
        }
        return from(nodes);
    }

    // Create a new json::Node from a map of values
    template<typename T>
    static std::shared_ptr<Node> from(const std::map<std::string, T>& values) {
        std::map<std::string, std::shared_ptr<Node>> nodes;
        for (auto it : values) {
            nodes[it.first] = Node::from(it.second);
        }
        return from(nodes);
    }

    // Create a new json::Node from a pair
    template<typename T, typename U>
    static std::shared_ptr<Node> from(const std::pair<T, U>& pair) {
        return from(array({
            Node::from(pair.first),
            Node::from(pair.second)
        }));
    }



    // Casts the json::Node to another json class (i.e. json::Object)
    // Throws an exception if it cannot be casted
    template<typename T>
    T* cast() {
        throw InvalidCastException(EXCTX);
    }



    // Returns a value from the node
    // Returns an empty std::optional if the node type is incorrect
    std::optional<char>                  getByte()       const;
    std::optional<unsigned char>         getUByte()      const;
    std::optional<short>                 getShort()      const;
    std::optional<unsigned short>        getUshort()     const;
    std::optional<int>                   getInt()        const;
    std::optional<unsigned int>          getUint()       const;
    std::optional<float>                 getFloat()      const;
    virtual std::optional<bool>          getBool()       const;
    virtual std::optional<long>          getLong()       const;
    virtual std::optional<unsigned long> getUlong()      const;
    virtual std::optional<double>        getDouble()     const;
    virtual std::optional<std::string>   getString()     const;
    virtual std::optional<array>         getArray()      const;
    virtual std::optional<dictionary>    getDictionary() const;

    // Returns a value from the node
    // Returns an empty std::optional if the node type is incorrect
    template<typename T>
    std::optional<T> get() {
        return std::optional<T>();
    }

    // Returns a value from the node
    // Returns a given fallbackValue if the node type is incorrect
    template<typename T>
    T getOr(T fallbackValue = T()) {
        return get<T>().value_or(fallbackValue);
    }

    // Returns a value from the node
    // Throws an exception if the node type is incorrect
    template<typename T>
    T getEx() {
        std::optional<T> v = get<T>();
        if (v.has_value()) {
            return v.value();
        } else {
            throw InvalidTypeException(EXCTX);
        }
    }

    // Load a value into a reference if possible
    // Returns true on success
    template<typename T>
    bool load(T& ref) {
        std::optional<T> value = this->get<T>();
        if (value.has_value()) {
            ref = value.value();
            return true;
        }

        return false;
    }



    // Gets the property of an object
    // Returns json::Undefined if the property doesn't exist
    std::shared_ptr<Node> getProperty(const std::string& key) const;

    // Gets the property of an object
    // Throws an exception if the property doesn't exist
    std::shared_ptr<Node> getPropertyEx(const std::string& key) const;

    // Gets the property of an object
    // Returns an empty std::optional if the property doesn't exist
    virtual std::optional<std::shared_ptr<Node>> getPropertyO(const std::string& key) const;

    // Checks if the property of an object exists
    bool hasProperty(const std::string& key) const;

    // Sets the property of an object
    // Returns this
    virtual Node* setProperty(const std::string& key, std::shared_ptr<Node> node);

    // Sets the property of an object
    // Returns this
    template<typename T>
    Node* setProperty(const std::string& key, T value) {
        return setProperty(key, Node::from(value));
    }



    // Gets the element of an array
    // Returns json::Undefined if the element doesn't exist
    std::shared_ptr<Node> getElement(std::size_t index) const;

    // Gets the element of an array
    // Throws an exception if the element doesn't exist
    std::shared_ptr<Node> getElementEx(std::size_t index) const;

    // Gets the element of an array
    // Returns an empty std::optional if the element doesn't exist
    virtual std::optional<std::shared_ptr<Node>> getElementO(std::size_t index) const;

    // Checks if the element of an array exists
    bool hasElement(std::size_t index) const;

    // Get the size of an array
    virtual std::size_t size() const;

    // Sets the element of an array
    // Returns this
    virtual Node* setElement(std::size_t index, std::shared_ptr<Node> node);

    // Sets the element of an array
    // Returns this
    template<typename T>
    Node* setElement(std::size_t index, T& value) {
        return setElement(index, Node::from(value));
    }
    


    // Converts the data to a nlohmann::json structure
    // Throws an exception on error
    virtual nlohmann::json toNlohmann() = 0;

    // Appends the JSON string to the stream
    // Throws an exception on error
    void toStream(std::ostream& stream);

    // Appends the JSON string to the stream
    // Throws an exception on error
    friend std::ostream& operator<<(std::ostream& stream, std::shared_ptr<Node> instance);

    // Returns a JSON string representation of the data
    // Throws an exception on error
    std::string toJSONString();

    // Saves the JSON data to file
    // Throws an exception on error
    void toFile(std::filesystem::path& filename);



    // Loads data from a nlohmann::json structure
    // (this should never fail)
    static std::shared_ptr<Node> fromNlohmann(const nlohmann::json& j);
    

    // Loads data from a JSON string
    // Returns json::Undefined when loading has failed
    static std::shared_ptr<Node> fromJSON(const std::string& json);

    // Loads data from a JSON file
    // Returns json::Undefined when loading has failed
    static std::shared_ptr<Node> fromFile(const std::filesystem::path& filename);


    // Loads data from a JSON string
    // Throws an exception when loading has failed
    static std::shared_ptr<Node> fromJSONEx(const std::string& json);

    // Loads data from a JSON file
    // Throws an exception when loading has failed
    static std::shared_ptr<Node> fromFileEx(const std::filesystem::path& filename);


    // Loads data from a JSON string
    // Returns an empty std::optional when loading has failed
    static std::optional<std::shared_ptr<Node>> fromJSONO(const std::string& json);

    // Loads data from a JSON file
    // Returns an empty std::optional when loading has failed
    static std::optional<std::shared_ptr<Node>> fromFileO(const std::filesystem::path& filename);


protected:
    Node(Type type);


private:
    Type type;

};

}

}
