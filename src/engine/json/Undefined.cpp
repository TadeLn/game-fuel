#include "Undefined.hpp"

#include "../exception/InvalidOperationException.hpp"



namespace tge {

namespace json {

nlohmann::json Undefined::toNlohmann() {
    throw InvalidOperationException("cannot save an undefined value", EXCTX);
}

std::shared_ptr<Undefined> Undefined::fromNlohmann(__attribute__ ((unused)) const nlohmann::json& j) {
    throw InvalidOperationException("cannot load an undefined value", EXCTX);
}



Undefined::Undefined() : Value(Node::Type::UNDEFINED) {}



std::string Undefined::toString() const {
    return "undefined";
}

}

}
