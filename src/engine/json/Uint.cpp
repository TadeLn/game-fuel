#include "Uint.hpp"



namespace tge {

namespace json {

std::optional<unsigned long> Uint::getUlong() const {
    return std::optional(value);
}

std::optional<long> Uint::getLong() const {
    return std::optional((long)value);
}



nlohmann::json Uint::toNlohmann() {
    return nlohmann::json(value);
}

std::shared_ptr<Uint> Uint::fromNlohmann(const nlohmann::json& j) {
    std::shared_ptr<Uint> result = std::make_shared<Uint>();
    result->value = j.get<unsigned long>();
    return result;
}



Uint::Uint(unsigned long value) : Value(Node::Type::UINT) {
    this->value = value;
}



std::string Uint::toString() const {
    return std::to_string(value);
}

}

}
