#include "Int.hpp"



namespace tge {

namespace json {

std::optional<long> Int::getLong() const {
    return std::optional(value);
}



nlohmann::json Int::toNlohmann() {
    return nlohmann::json(value);
}

std::shared_ptr<Int> Int::fromNlohmann(const nlohmann::json& j) {
    std::shared_ptr<Int> result = std::make_shared<Int>();
    result->value = j.get<long>();
    return result;
}



Int::Int(long value) : Value(Node::Type::INT) {
    this->value = value;
}



std::string Int::toString() const {
    return std::to_string(value);
}

}

}
