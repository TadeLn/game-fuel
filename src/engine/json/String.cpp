#include "String.hpp"



namespace tge {

namespace json {

std::optional<std::string> String::getString() const {
    return std::optional(value);
}



nlohmann::json String::toNlohmann() {
    return nlohmann::json(value);
}

std::shared_ptr<String> String::fromNlohmann(const nlohmann::json& j) {
    std::shared_ptr<String> result = std::make_shared<String>();
    result->value = j.get<std::string>();
    return result;
}



String::String(std::string value) : Value(Node::Type::STRING) {
    this->value = value;
}



std::string String::toString() const {
    return "\"" + value + "\"";
}

}

}
