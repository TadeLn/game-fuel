#include "Deserializable.hpp"



namespace tge {

namespace json {

bool Deserializable::loadJSON(std::shared_ptr<Node> json) {
    try {
        this->loadJSONEx(json);
        return true;
    } catch (tge::Exception& e) {
        return false;
    }
}

}

}
