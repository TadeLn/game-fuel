#pragma once


#include <vector>
#include <memory>

#include "Node.hpp"
#include "Value.hpp"



namespace tge {

namespace json {

// json::Array is an array of any type, like: [0, 3, 2, "text1", "text2", null, 4.56]
class Array : public Node {


public:
    array nodes;
    std::optional<array> getArray() const;

    std::optional<std::shared_ptr<Node>> getElementO(std::size_t index) const;
    std::size_t size() const;
    Node* setElement(std::size_t index, std::shared_ptr<Node> node);

    nlohmann::json toNlohmann();
    static std::shared_ptr<Array> fromNlohmann(const nlohmann::json& j);

    Array(array values = array());

    std::string toString() const;


};

}

}
