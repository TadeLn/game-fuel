#include "Serializable.hpp"



namespace tge {

namespace json {

std::string Serializable::toString() const {
    return this->toJSON()->toString();
}

}

}
