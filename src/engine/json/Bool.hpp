#pragma once

#include <memory>
#include <optional>
#include <string>

#include "Value.hpp"



namespace tge {

namespace json {

// json::Bool is a one-bit value: true or false
class Bool : public Value {


public:
    bool value;
    std::optional<bool> getBool() const;

    nlohmann::json toNlohmann();
    static std::shared_ptr<Bool> fromNlohmann(const nlohmann::json& j);

    Bool(bool value = false);

    std::string toString() const;


};

}

}
