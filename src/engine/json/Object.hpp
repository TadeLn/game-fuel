#pragma once

#include <map>
#include <string>
#include <optional>
#include <memory>

#include "Node.hpp"
#include "Value.hpp"



namespace tge {

namespace json {

// json::Object is an object of any type, like: {"a": 0, "b": "text1", "c": "text2", "d": 4.56}
class Object : public Node {


public:
    dictionary nodes;
    std::optional<dictionary> getDictionary() const;

    std::optional<std::shared_ptr<Node>> getPropertyO(const std::string& key) const;
    Node* setProperty(const std::string& key, std::shared_ptr<Node> node);

    nlohmann::json toNlohmann();
    static std::shared_ptr<Object> fromNlohmann(const nlohmann::json& j);

    Object(dictionary values = dictionary());

    std::string toString() const;


};

}

}