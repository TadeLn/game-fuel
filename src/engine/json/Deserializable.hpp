#pragma once

#include <any>

#include "Node.hpp"



namespace tge {

namespace json {

class Deserializable {


public:
    // Loads data from JSON into the object
    // Throws an exception when loading has failed
    virtual void loadJSONEx(std::shared_ptr<Node> json) = 0;

    // Loads data from JSON into the object
    // Returns false when loading has failed
    virtual bool loadJSON(std::shared_ptr<Node> json);


};

}

}
