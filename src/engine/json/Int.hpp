#pragma once

#include <memory>
#include <optional>
#include <string>

#include "Value.hpp"



namespace tge {

namespace json {

// json::Int is a number without a floating point, like 0, 3, -6, 9000000000
class Int : public Value {

public:
    unsigned long value;
    std::optional<long> getLong() const;

    nlohmann::json toNlohmann();
    static std::shared_ptr<Int> fromNlohmann(const nlohmann::json& j);

    Int(long value = 0);

    std::string toString() const;

};

}

}
