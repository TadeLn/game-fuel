#include "Array.hpp"


#include "../String.hpp"
#include "Null.hpp"



namespace tge {

namespace json {

std::optional<array> Array::getArray() const {
    return std::optional(nodes);
}



std::optional<std::shared_ptr<Node>> Array::getElementO(std::size_t index) const {
    if (index >= nodes.size()) {
        return std::optional<std::shared_ptr<Node>>();
    }
    return std::optional(nodes.at(index));
}

std::size_t Array::size() const {
    return nodes.size();
}

Node* Array::setElement(std::size_t index, std::shared_ptr<Node> node) {
    while (nodes.size() <= index) {
        nodes.push_back(std::make_shared<Null>());
    }
    nodes[index] = node;
    return this;
}



nlohmann::json Array::toNlohmann() {
    nlohmann::json j = nlohmann::json::array();
    for (auto it : nodes) {
        j.push_back(it->toNlohmann());
    }
    return j;
}

std::shared_ptr<Array> Array::fromNlohmann(const nlohmann::json& j) {
    std::shared_ptr<Array> result = std::make_shared<Array>();
    for (auto it = j.begin(); it != j.end(); ++it) {
        result->nodes.push_back(Node::fromNlohmann(it.value()));
    }
    return result;
}



Array::Array(array values) : Node(Node::Type::ARRAY) {
    nodes = values;
}



// WARNING: this function does not guarantee to return proper JSON
std::string Array::toString() const {
    return String::fromVector(nodes, [](std::shared_ptr<Node> value, __attribute__ ((unused)) std::size_t index){
        return value->toString();
    }, false);
}

}

}
