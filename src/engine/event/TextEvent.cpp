#include "TextEvent.hpp"

#include <sstream>

#include "../UTF8.hpp"


namespace tge {

std::uint32_t TextEvent::getCodepoint() const {
    return this->codepoint;
}

std::optional<char> TextEvent::getChar() const {
    if (this->codepoint < 128) {
        return std::optional((char) this->codepoint);
    }
    return std::optional<char>();
}

std::string TextEvent::getUTF8String() const {
    return UTF8::encodeUTF8(this->codepoint);
}

std::string TextEvent::toString() const {
    std::stringstream ss;
    ss << std::uppercase << std::hex << this->codepoint;
    return std::string("<TextEvent: '") + this->getUTF8String() + "' (U+" + ss.str() + ")>";
}

TextEvent::TextEvent(std::chrono::steady_clock::time_point timestamp, std::uint32_t codepoint)
    : Event(timestamp) {
    this->codepoint = codepoint;
}

}
