#pragma once

#include <optional>

#include "Event.hpp"
#include "../input/Keyboard.hpp"



namespace tge {

// A class for managing keyboard typinh events.
class TextEvent : public Event {

public:
    // Returns the unicode codepoint
    std::uint32_t getCodepoint() const;

    // Returns the ASCII character (or nothing, if a non-ASCII character was typed)
    std::optional<char> getChar() const;

    // Returns the typed character as a UTF8 string
    std::string getUTF8String() const;

    // Returns the string representation of the event
    virtual std::string toString() const;

    // Constructs a new keyboard key event
    TextEvent(std::chrono::steady_clock::time_point timestamp, std::uint32_t codepoint);

protected:
    std::uint32_t codepoint;

};

}
