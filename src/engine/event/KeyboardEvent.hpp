#pragma once

#include "Event.hpp"
#include "../input/Keyboard.hpp"



namespace tge {

// A class for managing keyboard key up and key down events.
class KeyboardEvent : public Event {

public:
    enum class KeyState {
        RELEASED = 0,
        PRESSED = 1
    };

    // Returns which key was pressed or released.
    Keyboard::Key getKey() const;

    // Returns the state of the key (pressed or released).
    KeyState getKeyState() const;

    // Returns the string representation of the event
    virtual std::string toString() const;

    // Constructs a new keyboard key event
    KeyboardEvent(std::chrono::steady_clock::time_point timestamp, Keyboard::Key key, KeyState keyState);

protected:
    Keyboard::Key key;
    KeyState keyState;

};

}
