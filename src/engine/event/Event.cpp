#include "Event.hpp"



namespace tge {

std::chrono::steady_clock::time_point Event::getTimestamp() const {
    return this->timestamp;
}

std::string Event::toString() const {
    return "<Event: time=" + std::to_string(
        std::chrono::duration_cast<std::chrono::milliseconds>(this->timestamp.time_since_epoch()).count()
    ) + ">";
}

Event::Event(std::chrono::steady_clock::time_point timestamp) {
    this->timestamp = timestamp;
}

}
