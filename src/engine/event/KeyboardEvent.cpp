#include "KeyboardEvent.hpp"



namespace tge {

Keyboard::Key KeyboardEvent::getKey() const {
    return this->key;
}

KeyboardEvent::KeyState KeyboardEvent::getKeyState() const {
    return this->keyState;
}

std::string KeyboardEvent::toString() const {
    return std::string("<KeyboardEvent: ") + (this->keyState == KeyState::PRESSED ? "PRESSED" : "RELEASED") + ", " + std::to_string((std::int32_t) this->key) + ">";
}

KeyboardEvent::KeyboardEvent(std::chrono::steady_clock::time_point timestamp, Keyboard::Key key, KeyboardEvent::KeyState keyState)
    : Event(timestamp) {
    this->key = key;
    this->keyState = keyState;
}

}
