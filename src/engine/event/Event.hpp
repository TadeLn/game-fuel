#pragma once

#include <chrono>

#include "../Stringable.hpp"



namespace tge {

// A generic class for storing information about various events
class Event : public Stringable {

public:
    // Returns the time point at which the event has happened
    std::chrono::steady_clock::time_point getTimestamp() const;

    // Returns the string representation of the event
    virtual std::string toString() const;

    // Constructs a new event by using a time point
    Event(std::chrono::steady_clock::time_point timestamp);

protected:
    std::chrono::steady_clock::time_point timestamp;

};

}
