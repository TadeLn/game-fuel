#pragma once

#include <string>


namespace tge {

// A general class for objects which can be turned into a string.
// Extend this class for your class to be easily usable while logging output by the engine.
class Stringable {

public:
    // Convert the object to a std::string
    virtual std::string toString() const = 0;

    // Convert the object to a std::string
    operator std::string() const;

    friend std::ostream& operator<<(std::ostream& stream, const Stringable& stringable);

};

}
