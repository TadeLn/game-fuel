#pragma once

#include <vector>
#include <string>

#include "event/KeyboardEvent.hpp"
#include "event/TextEvent.hpp"
#include "math/Vec2.hpp"
#include "Window.hpp"
#include "scene/Scene.hpp"
#include "log/Logger.hpp"



namespace tge {

class Application {

public:
    // Starts up the application.
    // Initializes the game engine and enters the main game loop.
    // Returns an exit code when the application will eventually stop:
    // 0: application exited normally,
    // other than 0: an error ocurred.
    int start(int argc, char** argv);

    // Stops the main loop of the application.
    void stop();

    // Returns true if the application is still running, or needs to be stopped.
    bool isRunning() const;

    // Get fps count
    std::uint32_t getFps() const;

    // Get object from active scene
    template<typename T>
    std::shared_ptr<T> getObject(UUID objectUUID) const {
        return std::dynamic_pointer_cast<T>(activeScene->getObject(objectUUID).lock());
    }

    // This method is called when the Application::start() method is called.
    // You may override this method to implement your own initialization functionality.
    virtual void onInit(const std::vector<std::string>& commandLineArgument, Window& win);

    // This method is called when the application loop is ended.
    // You may override this method to implement your own application close functionality.
    virtual void onDestroy(Window& win);

    // This method is called on every tick.
    // This method is executed on a separate thread to the Application::frame() method.
    virtual void onTick();

    // This method is called when the frame needs to be rendered.
    // This method is executed on a separate thread to the Application::tick() method.
    // deltaTime is the amount of time that has passed since the last time this function was called, in milliseconds
    virtual void onFrame(Window& win, double deltaTime);

    // This method is called when the application recieves a signal.
    virtual void onSignal(int signalNum);

    // This method is called on every keyboard event
    virtual void onKeyboard(const KeyboardEvent& event);

    // This method is called every time the user types a character on a keyboard
    virtual void onText(const TextEvent& event);

    Application(std::string applicationName, Vec2u windowSize = Vec2u(800, 600));

    static Application* getInstance();

protected:
    std::string applicationName;
    Scene* activeScene = nullptr;

private:
    static Application* instance;

    bool running = false;

    Vec2u initialWindowSize;
    Window* mainWindow = nullptr;

    std::chrono::steady_clock::time_point lastRender = std::chrono::steady_clock::now();
    std::chrono::steady_clock::time_point lastFpsCheck = std::chrono::steady_clock::now();
    std::uint32_t frameCount = 0;
    std::uint32_t fps = 0;

    Logger internalLogger = Logger("tge");

    static void renderHandler(Window* win);

};

}
