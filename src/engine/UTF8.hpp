#pragma once

#include <string>
#include <vector>



namespace tge {

class UTF8 {

public:
    static std::string encodeUTF8(std::uint32_t unicodeCodepoint);
    static std::string encodeUTF8(std::vector<std::uint32_t> unicodeCodepoints);


    struct UTF8DecodeResult {
        std::vector<std::uint32_t> codepoints;

        // How many bytes were left at the end of the decoding process
        std::uint8_t remainingBytes;
    };

    static UTF8DecodeResult decodeUTF8(std::string utf8string);

};

}
