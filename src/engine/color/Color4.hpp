#pragma once

#include <cstdint>

#include <SFML/Graphics.hpp>

#include "../Stringable.hpp"



namespace tge {

class Color3;

class Color4 : public Stringable {

public:
    std::uint8_t r;
    std::uint8_t g;
    std::uint8_t b;
    std::uint8_t a;

    // Get the string representation of the color object
    std::string toString() const;

    // Construct a new color object from three values
    Color4(std::uint8_t r, std::uint8_t g, std::uint8_t b);

    // Construct a new color object from four values
    Color4(std::uint8_t r, std::uint8_t g, std::uint8_t b, std::uint8_t a);

    // Construct a new color object from a hexadecimal code (0xrrggbbaa)
    Color4(std::uint32_t hex);

    // Convert Color3 to Color4
    Color4(const Color3& color);

    // Convert Color4 to Color3
    operator Color3() const;

    // Convert sf::Color to Color4
    Color4(const sf::Color& color);

    // Convert Color4 to sf::Color
    operator sf::Color() const;


    // Mix two colors (avg of rgb values)
    Color4 mix(const Color4& color) const;

    // Mix two colors (avg of rgb values)
    Color4 operator*(const Color4& color) const;


    static Color4 RED;
    static Color4 YELLOW;
    static Color4 GREEN;
    static Color4 CYAN;
    static Color4 BLUE;
    static Color4 MAGENTA;
    static Color4 BLACK;
    static Color4 WHITE;
    static Color4 GRAY;
    static Color4 TRANSPARENT;

};

}
