#include "Color3.hpp"

#include "Color4.hpp"



namespace tge {

std::string Color3::toString() const {
    return "<Color: (" + std::to_string(this->r) + ", " + std::to_string(this->g) + ", " + std::to_string(this->b) + ")>";
}

Color3::Color3(std::uint8_t r, std::uint8_t g, std::uint8_t b) {
    this->r = r;
    this->g = g;
    this->b = b;
}

Color3::Color3(std::uint32_t hex) {
    this->r = hex >> 16;
    this->g = hex >> 8;
    this->b = hex;
}

Color3::Color3(const Color4& color) {
    this->r = color.r;
    this->g = color.g;
    this->b = color.b;
}

Color3 Color3::RED     = Color3(0xff0000);
Color3 Color3::YELLOW  = Color3(0xffff00);
Color3 Color3::GREEN   = Color3(0x00ff00);
Color3 Color3::CYAN    = Color3(0x00ffff);
Color3 Color3::BLUE    = Color3(0x0000ff);
Color3 Color3::MAGENTA = Color3(0xff00ff);
Color3 Color3::BLACK   = Color3(0x000000);
Color3 Color3::WHITE   = Color3(0xffffff);
Color3 Color3::GRAY    = Color3(0x7f7f7f);

}