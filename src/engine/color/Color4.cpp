#include "Color4.hpp"

#include "Color3.hpp"



namespace tge {

std::string Color4::toString() const {
    return "<Color: (" + std::to_string(this->r) + ", " + std::to_string(this->g) + ", " + std::to_string(this->b) + ", " + std::to_string(this->a) + ")>";
}

Color4::Color4(std::uint8_t r, std::uint8_t g, std::uint8_t b) {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = 255;
}

Color4::Color4(std::uint8_t r, std::uint8_t g, std::uint8_t b, std::uint8_t a) {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = a;
}

Color4::Color4(std::uint32_t hex) {
    this->r = hex >> 24;
    this->g = hex >> 16;
    this->b = hex >> 8;
    this->a = hex;
}

Color4::Color4(const Color3& color) {
    this->r = color.r;
    this->g = color.g;
    this->b = color.b;
    this->a = 255;
}

Color4::operator Color3() const {
    return Color3(r, g, b);
}

Color4::Color4(const sf::Color& color) {
    this->r = color.r;
    this->g = color.g;
    this->b = color.b;
    this->a = color.a;
}

Color4::operator sf::Color() const {
    return sf::Color(r, g, b, a);
}



Color4 Color4::mix(const Color4& color) const {
    return Color4(
        (((std::uint32_t) this->r) + color.r) / 2,
        (((std::uint32_t) this->g) + color.g) / 2,
        (((std::uint32_t) this->b) + color.b) / 2,
        (((std::uint32_t) this->a) + color.a) / 2
    );
}

Color4 Color4::operator*(const Color4& color) const {
    return this->mix(color);
}



Color4 Color4::RED         = Color4(0xff0000ff);
Color4 Color4::YELLOW      = Color4(0xffff00ff);
Color4 Color4::GREEN       = Color4(0x00ff00ff);
Color4 Color4::CYAN        = Color4(0x00ffffff);
Color4 Color4::BLUE        = Color4(0x0000ffff);
Color4 Color4::MAGENTA     = Color4(0xff00ffff);
Color4 Color4::BLACK       = Color4(0x000000ff);
Color4 Color4::WHITE       = Color4(0xffffffff);
Color4 Color4::GRAY        = Color4(0x7f7f7fff);
Color4 Color4::TRANSPARENT = Color4(0x00000000);

}