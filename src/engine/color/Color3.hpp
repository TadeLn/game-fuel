#pragma once

#include <cstdint>

#include "../Stringable.hpp"



namespace tge {

class Color4;

class Color3 : public Stringable {

public:
    std::uint8_t r;
    std::uint8_t g;
    std::uint8_t b;

    // Get the string representation of the color object
    std::string toString() const;

    // Construct a new color object from three values
    Color3(std::uint8_t r, std::uint8_t g, std::uint8_t b);

    // Construct a new color object from a hexadecimal code (0x00rrggbb)
    Color3(std::uint32_t hex);

    // Convert Color4 to Color3
    Color3(const Color4& color);

    static Color3 RED;
    static Color3 YELLOW;
    static Color3 GREEN;
    static Color3 CYAN;
    static Color3 BLUE;
    static Color3 MAGENTA;
    static Color3 BLACK;
    static Color3 WHITE;
    static Color3 GRAY;

};

}
