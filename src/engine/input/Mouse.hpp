#pragma once

#include <array>

#include "../Window.hpp"
#include "../math/Vec2.hpp"



namespace tge {

class Mouse {

public:
    enum class Button {
        UNKNOWN = -1,

        LEFT = 0,
        RIGHT,
        MIDDLE,
        BUTTON_3,
        BUTTON_4,
        BUTTON_5,

        __BUTTON_COUNT
    };

    enum class Wheel {
        UNKNOWN = -1,
        VERTICAL = 0,
        HORIZONTAL
    };


    // Convert sf::Mouse::Button to tge::Mouse::Button
    static Button fromSfButton(sf::Mouse::Button button);

    // Convert tge::Mouse::Button to sf::Mouse::Button
    // [TODO]
    static sf::Mouse::Button toSfButton(Button button);


    // Convert sf::Mouse::Wheel to tge::Mouse::Wheel
    static Wheel fromSfWheel(sf::Mouse::Wheel wheel);

    // Convert tge::Mouse::Wheel to sf::Mouse::Wheel
    // [TODO]
    static sf::Mouse::Wheel toSfWheel(Wheel wheel);


    // Get mouse position relative to a window
    static Vec2i getPosition(const Window& relativeTo);

    // Get mouse position on the screen
    // [TODO]
    static Vec2i getAbsolutePosition();

    // Returns true if the specific mouse button is pressed
    static bool isButtonPressed(Button button);

    // Returns true if the mouse cursor is hovered over a window of the application
    // [TODO]
    static bool isHovered();


    // Changes the state of the mouse button and calls associated events with it
    // [TODO]
    static void changeButtonState(bool isPressed, Button button, Vec2i position);

    // Changes the hover state of the mouse and calls associated events with it
    // [TODO]
    static void changeHoverState(bool isHovered);

    // Changes the state of the mouse's position and calls associated events with it
    // [TODO]
    static void changePositionState(Vec2i position);

    // Calls events associated with scrolling the mouse wheel
    // [TODO]
    static void changeScrollState(Wheel wheel, double scroll, Vec2i position);

private:
    static Vec2i mousePositionState;
    static std::array<bool, (std::size_t) Button::__BUTTON_COUNT> mouseButtonState;
    static bool mouseHoverState;

};

}
