#pragma once

#include <cstdint>
#include <vector>



namespace tge {

class Gamepad {

public:
    enum class Button {
        BUTTON_0,
        BUTTON_1,
        BUTTON_2,
        BUTTON_3,
        BUTTON_4,
        BUTTON_5,
        BUTTON_6,
        BUTTON_7,
        BUTTON_8,
        BUTTON_9,
        BUTTON_10,
        BUTTON_11,
        BUTTON_12,
        BUTTON_13,
        BUTTON_14,
        BUTTON_15,
        BUTTON_16,
        BUTTON_17,
        BUTTON_18,
        BUTTON_19,
        BUTTON_20,
        BUTTON_21,
        BUTTON_22,
        BUTTON_23,
        BUTTON_24,
        BUTTON_25,
        BUTTON_26,
        BUTTON_27,
        BUTTON_28,
        BUTTON_29,
        BUTTON_30,
        BUTTON_31
    };


    // [TODO]
    bool isConnected();

    // [TODO]
    std::uint8_t getId();

    // [TODO]
    std::string getName();

    // [TODO]
    bool isButtonPressed(Gamepad::Button button);

    // [TODO]
    bool isButtonPressed(std::uint8_t button);


    // [TODO]
    static std::vector<std::uint8_t> getConnectedGamepads();

    // [TODO]
    static Gamepad getGamepad(std::uint8_t id);


protected:
    std::uint8_t gamepadId;


};

}
