#include "Keyboard.hpp"

#include <chrono>

#include "../Application.hpp"


namespace tge {

Keyboard::Key Keyboard::fromSfKey(sf::Keyboard::Key key) {
    switch (key) {
        case sf::Keyboard::Key::A:         return Key::A;
        case sf::Keyboard::Key::B:         return Key::B;
        case sf::Keyboard::Key::C:         return Key::C;
        case sf::Keyboard::Key::D:         return Key::D;
        case sf::Keyboard::Key::E:         return Key::E;
        case sf::Keyboard::Key::F:         return Key::F;
        case sf::Keyboard::Key::G:         return Key::G;
        case sf::Keyboard::Key::H:         return Key::H;
        case sf::Keyboard::Key::I:         return Key::I;
        case sf::Keyboard::Key::J:         return Key::J;
        case sf::Keyboard::Key::K:         return Key::K;
        case sf::Keyboard::Key::L:         return Key::L;
        case sf::Keyboard::Key::M:         return Key::M;
        case sf::Keyboard::Key::N:         return Key::N;
        case sf::Keyboard::Key::O:         return Key::O;
        case sf::Keyboard::Key::P:         return Key::P;
        case sf::Keyboard::Key::Q:         return Key::Q;
        case sf::Keyboard::Key::R:         return Key::R;
        case sf::Keyboard::Key::S:         return Key::S;
        case sf::Keyboard::Key::T:         return Key::T;
        case sf::Keyboard::Key::U:         return Key::U;
        case sf::Keyboard::Key::V:         return Key::V;
        case sf::Keyboard::Key::W:         return Key::W;
        case sf::Keyboard::Key::X:         return Key::X;
        case sf::Keyboard::Key::Y:         return Key::Y;
        case sf::Keyboard::Key::Z:         return Key::Z;
        case sf::Keyboard::Key::Num0:      return Key::NUM_0;
        case sf::Keyboard::Key::Num1:      return Key::NUM_1;
        case sf::Keyboard::Key::Num2:      return Key::NUM_2;
        case sf::Keyboard::Key::Num3:      return Key::NUM_3;
        case sf::Keyboard::Key::Num4:      return Key::NUM_4;
        case sf::Keyboard::Key::Num5:      return Key::NUM_5;
        case sf::Keyboard::Key::Num6:      return Key::NUM_6;
        case sf::Keyboard::Key::Num7:      return Key::NUM_7;
        case sf::Keyboard::Key::Num8:      return Key::NUM_8;
        case sf::Keyboard::Key::Num9:      return Key::NUM_9;
        case sf::Keyboard::Key::Escape:    return Key::ESCAPE;
        case sf::Keyboard::Key::LControl:  return Key::CONTROL_LEFT;
        case sf::Keyboard::Key::LShift:    return Key::SHIFT_LEFT;
        case sf::Keyboard::Key::LAlt:      return Key::ALT_LEFT;
        case sf::Keyboard::Key::LSystem:   return Key::SUPER_LEFT;
        case sf::Keyboard::Key::RControl:  return Key::CONTROL_RIGHT;
        case sf::Keyboard::Key::RShift:    return Key::SHIFT_RIGHT;
        case sf::Keyboard::Key::RAlt:      return Key::ALT_RIGHT;
        case sf::Keyboard::Key::RSystem:   return Key::SUPER_RIGHT;
        case sf::Keyboard::Key::Menu:      return Key::MENU;
        case sf::Keyboard::Key::LBracket:  return Key::LEFT_SQUARE_BRACKET;
        case sf::Keyboard::Key::RBracket:  return Key::RIGHT_SQUARE_BRACKET;
        case sf::Keyboard::Key::Semicolon: return Key::SEMICOLON;
        case sf::Keyboard::Key::Comma:     return Key::COMMA;
        case sf::Keyboard::Key::Period:    return Key::DOT;
        case sf::Keyboard::Key::Quote:     return Key::APOSTROPHE;
        case sf::Keyboard::Key::Slash:     return Key::SLASH;
        case sf::Keyboard::Key::Backslash: return Key::BACKSLASH;
        case sf::Keyboard::Key::Tilde:     return Key::GRAVE;
        case sf::Keyboard::Key::Equal:     return Key::EQUALS_SIGN;
        case sf::Keyboard::Key::Hyphen:    return Key::HYPHEN_MINUS;
        case sf::Keyboard::Key::Space:     return Key::SPACE;
        case sf::Keyboard::Key::Enter:     return Key::RETURN;
        case sf::Keyboard::Key::Backspace: return Key::BACKSPACE;
        case sf::Keyboard::Key::Tab:       return Key::TAB;
        case sf::Keyboard::Key::PageUp:    return Key::PAGE_UP;
        case sf::Keyboard::Key::PageDown:  return Key::PAGE_DOWN;
        case sf::Keyboard::Key::End:       return Key::END;
        case sf::Keyboard::Key::Home:      return Key::HOME;
        case sf::Keyboard::Key::Insert:    return Key::INSERT;
        case sf::Keyboard::Key::Delete:    return Key::DELETE;
        case sf::Keyboard::Key::Add:       return Key::NUMPAD_ADD;
        case sf::Keyboard::Key::Subtract:  return Key::NUMPAD_SUBTRACT;
        case sf::Keyboard::Key::Multiply:  return Key::NUMPAD_MULTIPLY;
        case sf::Keyboard::Key::Divide:    return Key::NUMPAD_DIVIDE;
        case sf::Keyboard::Key::Left:      return Key::ARROW_LEFT;
        case sf::Keyboard::Key::Right:     return Key::ARROW_RIGHT;
        case sf::Keyboard::Key::Up:        return Key::ARROW_UP;
        case sf::Keyboard::Key::Down:      return Key::ARROW_DOWN;
        case sf::Keyboard::Key::Numpad0:   return Key::NUMPAD_0;
        case sf::Keyboard::Key::Numpad1:   return Key::NUMPAD_1;
        case sf::Keyboard::Key::Numpad2:   return Key::NUMPAD_2;
        case sf::Keyboard::Key::Numpad3:   return Key::NUMPAD_3;
        case sf::Keyboard::Key::Numpad4:   return Key::NUMPAD_4;
        case sf::Keyboard::Key::Numpad5:   return Key::NUMPAD_5;
        case sf::Keyboard::Key::Numpad6:   return Key::NUMPAD_6;
        case sf::Keyboard::Key::Numpad7:   return Key::NUMPAD_7;
        case sf::Keyboard::Key::Numpad8:   return Key::NUMPAD_8;
        case sf::Keyboard::Key::Numpad9:   return Key::NUMPAD_9;
        case sf::Keyboard::Key::F1:        return Key::F1;
        case sf::Keyboard::Key::F2:        return Key::F2;
        case sf::Keyboard::Key::F3:        return Key::F3;
        case sf::Keyboard::Key::F4:        return Key::F4;
        case sf::Keyboard::Key::F5:        return Key::F5;
        case sf::Keyboard::Key::F6:        return Key::F6;
        case sf::Keyboard::Key::F7:        return Key::F7;
        case sf::Keyboard::Key::F8:        return Key::F8;
        case sf::Keyboard::Key::F9:        return Key::F9;
        case sf::Keyboard::Key::F10:       return Key::F10;
        case sf::Keyboard::Key::F11:       return Key::F11;
        case sf::Keyboard::Key::F12:       return Key::F12;
        case sf::Keyboard::Key::F13:       return Key::F13;
        case sf::Keyboard::Key::F14:       return Key::F14;
        case sf::Keyboard::Key::F15:       return Key::F15;
        case sf::Keyboard::Key::Pause:     return Key::PAUSE_BREAK;
        default:                           return Key::UNKNOWN;
    }
}

bool Keyboard::isPressed(Key key) {
    return Keyboard::keyboardState[(std::size_t) key];
}

void Keyboard::changeState(Key key, bool isPressed) {
    // Do not fire events from key repeat
    if (keyboardState[(std::size_t) key] == isPressed) {
        return;
    }

    keyboardState[(std::size_t) key] = isPressed;
    Application::getInstance()->onKeyboard(
        KeyboardEvent(
            std::chrono::steady_clock::now(),
            key,
            isPressed ? KeyboardEvent::KeyState::PRESSED : KeyboardEvent::KeyState::RELEASED
        )
    );
}

void Keyboard::releaseAllKeys() {
    for (std::size_t keyIndex = 0; keyIndex < (std::size_t) Key::__KEY_COUNT; keyIndex++) {
        changeState((Key) keyIndex, false);
    }
}

std::array<bool, (std::size_t) Keyboard::Key::__KEY_COUNT> Keyboard::keyboardState;

}
