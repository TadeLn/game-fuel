#pragma once

#include <array>

#include <SFML/Window.hpp>



namespace tge {

class Keyboard {

public:
    enum class Key {
        UNKNOWN = -1,

        ESCAPE,
        F1,
        F2,
        F3,
        F4,
        F5,
        F6,
        F7,
        F8,
        F9,
        F10,
        F11,
        F12,
        F13,
        F14,
        F15,
        F16,
        F17,
        F18,
        F19,
        F20,
        F21,
        F22,
        F23,
        F24,

        NUM_1,
        NUM_2,
        NUM_3,
        NUM_4,
        NUM_5,
        NUM_6,
        NUM_7,
        NUM_8,
        NUM_9,
        NUM_0,

        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T,
        U,
        V,
        W,
        X,
        Y,
        Z,

        SPACE,
        GRAVE,
        HYPHEN_MINUS,
        EQUALS_SIGN,
        BACKSPACE,
        TAB,
        LEFT_SQUARE_BRACKET,
        RIGHT_SQUARE_BRACKET,
        RETURN,
        CAPS_LOCK,
        SEMICOLON,
        APOSTROPHE,
        BACKSLASH,
        COMMA,
        DOT,
        SLASH,
        MENU,

        SHIFT_LEFT,
        SHIFT_RIGHT,
        CONTROL_LEFT,
        CONTROL_RIGHT,
        ALT_LEFT,
        ALT_RIGHT,
        SUPER_LEFT,
        SUPER_RIGHT,

        PRINT_SCREEN,
        SCROLL_LOCK,
        PAUSE_BREAK,
        INSERT,
        BEGIN, // Middle numpad button
        DELETE,
        HOME,
        END,
        PAGE_UP,
        PAGE_DOWN,

        ARROW_LEFT,
        ARROW_UP,
        ARROW_RIGHT,
        ARROW_DOWN,

        NUM_LOCK,
        NUMPAD_ADD,
        NUMPAD_SUBTRACT,
        NUMPAD_MULTIPLY,
        NUMPAD_DIVIDE,
        NUMPAD_ENTER,
        NUMPAD_DOT,
        NUMPAD_0,
        NUMPAD_1,
        NUMPAD_2,
        NUMPAD_3,
        NUMPAD_4,
        NUMPAD_5,
        NUMPAD_6,
        NUMPAD_7,
        NUMPAD_8,
        NUMPAD_9,

        __KEY_COUNT
    };


    // Convert sf::Keyboard::Key to tge::Keyboard::Key
    static Key fromSfKey(sf::Keyboard::Key key);

    // Convert tge::Keyboard::Key to sf::Keyboard::Key
    // [TODO]
    static sf::Keyboard::Key toSfKey(Key key);


    // Returns true if the key is pressed
    static bool isPressed(Key key);
    

    // Changes the state of the key on the keyboard
    static void changeState(Key key, bool isPressed);

    // Releases all keys on keyboard
    static void releaseAllKeys();

private:
    static std::array<bool, (std::size_t) Key::__KEY_COUNT> keyboardState;

};

}
