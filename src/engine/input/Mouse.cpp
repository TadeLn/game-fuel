#include "Mouse.hpp"



namespace tge {

Mouse::Button Mouse::fromSfButton(sf::Mouse::Button button) {
    switch (button) {
        case sf::Mouse::Button::Left:     return Button::LEFT;
        case sf::Mouse::Button::Right:    return Button::RIGHT;
        case sf::Mouse::Button::Middle:   return Button::MIDDLE;
        case sf::Mouse::Button::XButton1: return Button::BUTTON_4;
        case sf::Mouse::Button::XButton2: return Button::BUTTON_5;
        default:                          return Button::UNKNOWN;
    }
}

Mouse::Wheel Mouse::fromSfWheel(sf::Mouse::Wheel wheel) {
    switch (wheel) {
        case sf::Mouse::Wheel::VerticalWheel:   return Wheel::VERTICAL;
        case sf::Mouse::Wheel::HorizontalWheel: return Wheel::HORIZONTAL;
        default:                                return Wheel::UNKNOWN;
    }
}



Vec2i Mouse::getPosition(const Window& relativeTo) {
    sf::Vector2i position = sf::Mouse::getPosition(relativeTo.getSfWindow());
    return Vec2i(position.x, position.y);
}

bool Mouse::isButtonPressed(Button button) {
    return Mouse::mouseButtonState[(std::size_t) button];
}



void Mouse::changeButtonState(bool isPressed, Button button, Vec2i position) {
    // [TODO] dispatch mouse button events
    Mouse::mousePositionState = position;
    Mouse::mouseButtonState[(std::size_t) button] = isPressed;
}

void Mouse::changeHoverState(bool isHovered) {
    // [TODO] dispatch mouse hover events
    Mouse::mouseHoverState = isHovered;
}

void Mouse::changePositionState(Vec2i position) {
    // [TODO] dispatch mouse position events
    Mouse::mousePositionState = position;
}

void Mouse::changeScrollState(__attribute__ ((unused)) Wheel wheel, __attribute__ ((unused)) double scroll, Vec2i position) {
    // [TODO] dispatch scroll events
    Mouse::mousePositionState = position;
}

Vec2i Mouse::mousePositionState;
std::array<bool, (std::size_t) Mouse::Button::__BUTTON_COUNT> Mouse::mouseButtonState;
bool Mouse::mouseHoverState;

};
