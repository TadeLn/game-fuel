#pragma once

#include "../Stringable.hpp"


namespace tge {

class Date : public Stringable {

public:
    std::string toString() const;

    static Date now();

    Date(std::int64_t timestamp);

private:
    std::int64_t timestamp;

};

}
