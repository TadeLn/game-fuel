#include "Date.hpp"

#include <chrono>


namespace tge {

std::string Date::toString() const {
    std::time_t time = std::time({});
    char timeString[std::size("yyyy-mm-ddThh:mm:ssZ")];
    std::strftime(std::data(timeString), std::size(timeString), "%FT%TZ", std::gmtime(&time));
    return timeString;
    // return std::string("<Date: ") + std::to_string(this->timestamp) + std::string(">");
}

 
Date Date::now() {

    return Date(
        std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch()
        ).count()
    );
}


Date::Date(std::int64_t timestamp) {
    this->timestamp = timestamp;
}

}
