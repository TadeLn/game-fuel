#include "LogFile.hpp"


namespace tge {

void LogFile::write(std::string text) {
    file << text;
}

void LogFile::writeLine(std::string text) {
    write(text + "\n");
}



LogFile::LogFile(std::filesystem::path filePath) {
    this->filePath = filePath;
    file.open(this->filePath, std::ios::out | std::ios::app);
}

LogFile::~LogFile() {
    file.close();
}

}
