#pragma once

#include <string>

#include "../Stringable.hpp"


namespace tge {

class ANSICodes : public Stringable {

public:
    enum class ScreenMode {
        M_40_25_MONOCHROME_TEXT = 0,
        M_40_25_COLOR_TEXT = 1,
        M_80_25_MONOCHROME_TEXT = 2,
        M_80_25_COLOR_TEXT = 3,
        M_320_200_4COLOR_GFX = 4,
        M_320_200_MONOCHROME_GFX = 5,
        M_640_200_MONOCHROME_GFX = 6,
        M_ENABLE_LINE_WRAP = 7,

        M_320_200_COLOR_GFX = 13,
        M_640_200_16COLOR_GFX = 14,
        M_640_350_MONOCHROME_GFX = 15,
        M_640_350_16COLOR_GFX = 16,
        M_640_480_MONOCHROME_GFX = 17,
        M_640_480_16COLOR_GFX = 18,
        M_320_200_256COLOR_GFX = 19,
    };

    ANSICodes();
    ANSICodes(const ANSICodes& ansiCodes);
    ANSICodes operator=(const ANSICodes& ansiCodes);

    ANSICodes cursorHome() const;
    ANSICodes cursorMoveTo(std::uint32_t line, std::uint32_t column) const;
    ANSICodes cursorUp(std::uint32_t count) const;
    ANSICodes cursorDown(std::uint32_t count) const;
    ANSICodes cursorLeft(std::uint32_t count) const;
    ANSICodes cursorRight(std::uint32_t count) const;
    ANSICodes cursorLineUp(std::uint32_t count) const;
    ANSICodes cursorLineDown(std::uint32_t count) const;
    ANSICodes cursorColumn(std::uint32_t column) const;
    ANSICodes cursorRequest() const;
    ANSICodes cursorUpScroll() const;
    ANSICodes cursorSave() const;
    ANSICodes cursorRestore() const;
    ANSICodes cursorSaveSCO() const;
    ANSICodes cursorRestoreSCO() const;

    ANSICodes eraseToScerenEnd() const;
    ANSICodes eraseToScreenStart() const;
    ANSICodes eraseScreen() const;
    ANSICodes eraseSavedLines() const;
    ANSICodes eraseToLineEnd() const;
    ANSICodes eraseToLineStart() const;
    ANSICodes eraseLine() const;

    ANSICodes gfxReset() const;
    ANSICodes gfxBold() const;
    ANSICodes gfxDim() const;
    ANSICodes gfxBoldDimEnd() const;
    ANSICodes gfxItalic() const;
    ANSICodes gfxItalicEnd() const;
    ANSICodes gfxUnderline() const;
    ANSICodes gfxUnderlineEnd() const;
    ANSICodes gfxBlink() const;
    ANSICodes gfxBlinkEnd() const;
    ANSICodes gfxInverse() const;
    ANSICodes gfxInverseEnd() const;
    ANSICodes gfxHidden() const;
    ANSICodes gfxHiddenEnd() const;
    ANSICodes gfxStrikethrough() const;
    ANSICodes gfxStrikethroughEnd() const;

    ANSICodes colorBlack(bool bright = false) const;
    ANSICodes colorRed(bool bright = false) const;
    ANSICodes colorGreen(bool bright = false) const;
    ANSICodes colorYellow(bool bright = false) const;
    ANSICodes colorBlue(bool bright = false) const;
    ANSICodes colorMagenta(bool bright = false) const;
    ANSICodes colorCyan(bool bright = false) const;
    ANSICodes colorWhite(bool bright = false) const;
    ANSICodes colorDefault(bool bright = false) const;
    ANSICodes colorReset(bool bright = false) const;
    ANSICodes colorBackgroundBlack(bool bright = false) const;
    ANSICodes colorBackgroundRed(bool bright = false) const;
    ANSICodes colorBackgroundGreen(bool bright = false) const;
    ANSICodes colorBackgroundYellow(bool bright = false) const;
    ANSICodes colorBackgroundBlue(bool bright = false) const;
    ANSICodes colorBackgroundMagenta(bool bright = false) const;
    ANSICodes colorBackgroundCyan(bool bright = false) const;
    ANSICodes colorBackgroundWhite(bool bright = false) const;
    ANSICodes colorBackgroundDefault(bool bright = false) const;
    ANSICodes colorBackgroundReset(bool bright = false) const;
    ANSICodes color(std::uint8_t colorIndex) const;
    ANSICodes colorBackground(std::uint8_t colorIndex) const;
    ANSICodes color(std::uint8_t r, std::uint8_t g, std::uint8_t b) const;
    ANSICodes colorBackground(std::uint8_t r, std::uint8_t g, std::uint8_t b) const;

    ANSICodes screenMode(ScreenMode mode) const;

    ANSICodes p_cursorInvisible() const;
    ANSICodes p_cursorVisible() const;
    ANSICodes p_screenRestore() const;
    ANSICodes p_screenSave() const;
    ANSICodes p_alternativeBufferEnable() const;
    ANSICodes p_alternativeBufferDisable() const;

    std::string toString() const;

private:
    std::string result;
    ANSICodes copyAndAppend(std::string stringToAppend) const;

};

}
