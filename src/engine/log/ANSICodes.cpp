#include "ANSICodes.hpp"


namespace tge {

ANSICodes::ANSICodes() {
    this->result = "";
}

ANSICodes::ANSICodes(const ANSICodes& ansiCodes) {
    this->result = ansiCodes.result;
}

ANSICodes ANSICodes::operator=(const ANSICodes& ansiCodes) {
    this->result = ansiCodes.result;
    return ANSICodes(ansiCodes);
}



ANSICodes ANSICodes::cursorHome() const {
    return copyAndAppend("[H");
}

ANSICodes ANSICodes::cursorMoveTo(std::uint32_t line, std::uint32_t column) const {
    return copyAndAppend("[" + std::to_string(line) + ";" + std::to_string(column) + "H");
}

ANSICodes ANSICodes::cursorUp(std::uint32_t count) const {
    return copyAndAppend("[" + std::to_string(count) + "A");
}



ANSICodes ANSICodes::gfxReset() const {
    return copyAndAppend("[0m");
}



ANSICodes ANSICodes::colorBlack(bool bright) const {
    return copyAndAppend(std::string() + "[" + (bright ? "1;" : "") + "30m");
}

ANSICodes ANSICodes::colorRed(bool bright) const {
    return copyAndAppend(std::string() + "[" + (bright ? "1;" : "") + "31m");
}

ANSICodes ANSICodes::colorGreen(bool bright) const {
    return copyAndAppend(std::string() + "[" + (bright ? "1;" : "") + "32m");
}

ANSICodes ANSICodes::colorYellow(bool bright) const {
    return copyAndAppend(std::string() + "[" + (bright ? "1;" : "") + "33m");
}

ANSICodes ANSICodes::colorBlue(bool bright) const {
    return copyAndAppend(std::string() + "[" + (bright ? "1;" : "") + "34m");
}

ANSICodes ANSICodes::colorMagenta(bool bright) const {
    return copyAndAppend(std::string() + "[" + (bright ? "1;" : "") + "35m");
}

ANSICodes ANSICodes::colorCyan(bool bright) const {
    return copyAndAppend(std::string() + "[" + (bright ? "1;" : "") + "36m");
}

ANSICodes ANSICodes::colorWhite(bool bright) const {
    return copyAndAppend(std::string() + "[" + (bright ? "1;" : "") + "37m");
}

ANSICodes ANSICodes::colorDefault(bool bright) const {
    return copyAndAppend(std::string() + "[" + (bright ? "1;" : "") + "39m");
}

ANSICodes ANSICodes::colorReset(bool bright) const {
    return copyAndAppend(std::string() + "[" + (bright ? "1;" : "") + "0m");
}




std::string ANSICodes::toString() const {
    return this->result;
}



ANSICodes ANSICodes::copyAndAppend(std::string stringToAppend) const {
    ANSICodes codes;
    codes.result = this->result + "\x1B" + stringToAppend;
    return codes;
}

}