#include "Logger.hpp"

#include <iostream>
#include <sstream>

#include "ANSICodes.hpp"
#include "../util/Date.hpp"



namespace tge {

Logger::Logger(std::string name) {
    this->loggerName = name;
}

Logger::Logger(std::string name, std::shared_ptr<LogFile> logFile) 
    : Logger(name) {
    this->logFile = logFile;
}



void Logger::debug(std::string message) {
    this->write(message, LogLevel::DEBUG);
}

void Logger::debug(std::string message, std::string file, std::uint32_t line) {
    this->write(message, LogLevel::DEBUG, std::optional(std::pair(file, line)));
}

void Logger::log(std::string message) {
    this->write(message, LogLevel::LOG);
}

void Logger::log(std::string message, std::string file, std::uint32_t line) {
    this->write(message, LogLevel::LOG, std::optional(std::pair(file, line)));
}

void Logger::info(std::string message) {
    this->write(message, LogLevel::INFO);
}

void Logger::info(std::string message, std::string file, std::uint32_t line) {
    this->write(message, LogLevel::INFO, std::optional(std::pair(file, line)));
}

void Logger::warning(std::string message) {
    this->write(message, LogLevel::WARNING);
}

void Logger::warning(std::string message, std::string file, std::uint32_t line) {
    this->write(message, LogLevel::WARNING, std::optional(std::pair(file, line)));
}

void Logger::error(std::string message) {
    this->write(message, LogLevel::ERROR);
}

void Logger::error(std::string message, std::string file, std::uint32_t line) {
    this->write(message, LogLevel::ERROR, std::optional(std::pair(file, line)));
}



Logger Logger::_internalEngineLogger = Logger("tge");



void Logger::write(std::string message, LogLevel logLevel) {
    this->write(message, logLevel, std::optional<std::pair<std::string, std::uint32_t>>());
}

void Logger::write(std::string message, LogLevel logLevel, std::optional<std::pair<std::string, std::uint32_t>> extraContext) {
    std::ostream& consoleStream = logLevel <= LogLevel::ERROR ? std::cerr : std::cout;
    
    ANSICodes color;
    std::string logLevelName = "";
    switch (logLevel) {
        case LogLevel::NONE:
            break;

        case LogLevel::ERROR:
            color = ANSICodes().colorRed(true);
            logLevelName = "ERR";
            break;

        case LogLevel::WARNING:
            color = ANSICodes().colorYellow();
            logLevelName = "WARN";
            break;

        case LogLevel::INFO:
            color = ANSICodes().colorBlue();
            logLevelName = "INFO";
            break;

        case LogLevel::LOG:
            break;

        case LogLevel::DEBUG:
            color = ANSICodes().colorMagenta();
            logLevelName = "DEBUG";
            break;

        default:
            break;
    }

    ANSICodes reset;
    if (!color.toString().empty()) {
        reset = ANSICodes().gfxReset();
    }

    std::string logLevelNameText = logLevelName.empty() ? "" : ("[" + logLevelName + "] ");
    std::string extraContextText = extraContext.has_value() ? ("[" + extraContext.value().first + ":" + std::to_string(extraContext.value().second) + "] ") : "";

    std::stringstream ss;
    ss << "[" << Date::now() << "] " << "(" << this->loggerName << ") " << logLevelNameText << extraContextText << message;

    consoleStream << color << ss.str() << reset << "\n";
    if (logFile) {
        logFile->writeLine(ss.str());
    }
}


}
