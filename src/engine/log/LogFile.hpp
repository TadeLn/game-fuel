#pragma once

#include <fstream>
#include <filesystem>


namespace tge {

class LogFile {

public:
    void write(std::string text);
    void writeLine(std::string text);

    LogFile(std::filesystem::path filePath);
    ~LogFile();

protected:
    std::filesystem::path filePath;
    std::ofstream file;

};

}
