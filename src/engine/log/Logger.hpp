#pragma once

#include <string>
#include <memory>
#include <optional>

#include "LogFile.hpp"
#include "LogLevel.hpp"


#ifndef EXCTX
#define EXCTX std::string(__FILE__), __LINE__
#endif


namespace tge {

class Logger {

public:
    Logger(std::string name);
    Logger(std::string name, std::shared_ptr<LogFile> logFile);

    void debug(std::string message);
    void debug(std::string message, std::string file, std::uint32_t line);
    void log(std::string message);
    void log(std::string message, std::string file, std::uint32_t line);
    void info(std::string message);
    void info(std::string message, std::string file, std::uint32_t line);
    void warning(std::string message);
    void warning(std::string message, std::string file, std::uint32_t line);
    void error(std::string message);
    void error(std::string message, std::string file, std::uint32_t line);

    static Logger _internalEngineLogger;

private:
    void write(std::string message, LogLevel logLevel);
    void write(std::string message, LogLevel logLevel, std::optional<std::pair<std::string, std::uint32_t>> extraContext);

    std::string loggerName;
    std::shared_ptr<LogFile> logFile;

};

}
