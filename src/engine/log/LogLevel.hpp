#pragma once



namespace tge {

enum class LogLevel {
    NONE,
    ERROR,
    WARNING,
    INFO,
    LOG,
    DEBUG
};

}
