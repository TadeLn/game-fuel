#pragma once

#include <cstdint>

#include "Vec2.hpp"

namespace tge
{

    template <typename T, typename U>
    class Rect : public tge::Stringable
    {

    public:
        T x;
        T y;
        U w;
        U h;

        Vec2<T> getPosition() const
        {
            return Vec2(x, y);
        }

        Vec2<U> getSize() const
        {
            return Vec2(w, h);
        }

        Vec2<U> getCenter() const
        {
            return Vec2(x + (w / 2), y + (h / 2));
        }

        Rect()
        {
            this->x = T();
            this->y = T();
            this->w = U();
            this->h = U();
        }

        Rect(T x, T y, U w, U h)
        {
            this->x = x;
            this->y = y;
            this->w = w;
            this->h = h;
        }

        Rect(const Vec2<T> &pos, U w, U h)
        {
            this->x = pos.x;
            this->y = pos.y;
            this->w = w;
            this->h = h;
        }

        Rect(T x, T y, const Vec2<U> &size)
        {
            this->x = x;
            this->y = y;
            this->w = size.x;
            this->h = size.y;
        }

        Rect(const Vec2<T> &pos, const Vec2<U> &size)
        {
            this->x = pos.x;
            this->y = pos.y;
            this->w = size.x;
            this->h = size.y;
        }

        template <typename V, typename W>
        Rect(const Rect<V, W> &rect)
        {
            this->x = rect.x;
            this->y = rect.y;
            this->w = rect.w;
            this->h = rect.h;
        }

        std::string toString() const
        {
            return "(" + std::to_string(this->x) + ", " + std::to_string(this->y) + ", " + std::to_string(this->w) + ", " + std::to_string(this->h) + ")";
        }
    };

    typedef Rect<float, float> Rectf;
    typedef Rect<double, double> Rectd;
    typedef Rect<std::int32_t, std::uint32_t> Recti;
    typedef Rect<std::int64_t, std::uint64_t> Rectl;

}
