#pragma once

#include <cstdint>

#include <SFML/Graphics.hpp>

#include "../Stringable.hpp"

namespace tge
{

    template <typename T>
    class Vec2 : public Stringable
    {

    public:
        T x;
        T y;

        Vec2()
        {
            this->x = T();
            this->y = T();
        }

        Vec2(T x, T y)
        {
            this->x = x;
            this->y = y;
        }

        template <typename U, typename V>
        Vec2(U x, V y)
        {
            this->x = x;
            this->y = y;
        }

        template <typename U>
        Vec2(const Vec2<U> &vector)
        {
            this->x = vector.x;
            this->y = vector.y;
        }

        Vec2 operator+(const Vec2 &vector) const
        {
            return Vec2(this->x + vector.x, this->y + vector.y);
        }

        Vec2 operator-(const Vec2 &vector) const
        {
            return Vec2(this->x - vector.x, this->y - vector.y);
        }

        Vec2 operator*(const Vec2 &vector) const
        {
            return Vec2(this->x * vector.x, this->y * vector.y);
        }

        Vec2 operator/(const Vec2 &vector) const
        {
            return Vec2(this->x / vector.x, this->y / vector.y);
        }

        template <typename U>
        Vec2 operator+(const U &value) const
        {
            return Vec2(this->x + value, this->y + value);
        }

        template <typename U>
        Vec2 operator-(const U &value) const
        {
            return Vec2(this->x - value, this->y - value);
        }

        template <typename U>
        Vec2 operator*(const U &value) const
        {
            return Vec2(this->x * value, this->y * value);
        }

        template <typename U>
        Vec2 operator/(const U &value) const
        {
            return Vec2(this->x / value, this->y / value);
        }

        void operator+=(const Vec2 &vector)
        {
            this->x += vector.x;
            this->y += vector.y;
        }

        void operator-=(const Vec2 &vector)
        {
            this->x -= vector.x;
            this->y -= vector.y;
        }

        void operator*=(const Vec2 &vector)
        {
            this->x *= vector.x;
            this->y *= vector.y;
        }

        void operator/=(const Vec2 &vector)
        {
            this->x /= vector.x;
            this->y /= vector.y;
        }

        template <typename U>
        void operator+=(const U &value)
        {
            this->x += value;
            this->y += value;
        }

        template <typename U>
        void operator-=(const U &value)
        {
            this->x -= value;
            this->y -= value;
        }

        template <typename U>
        void operator*=(const U &value)
        {
            this->x *= value;
            this->y *= value;
        }

        template <typename U>
        void operator/=(const U &value)
        {
            this->x /= value;
            this->y /= value;
        }

        template <typename U>
        operator sf::Vector2<U>()
        {
            return sf::Vector2<U>(this->x, this->y);
        }

        std::string toString() const
        {
            return "(" + std::to_string(this->x) + ", " + std::to_string(this->y) + ")";
        }
    };

    typedef Vec2<float> Vec2f;
    typedef Vec2<double> Vec2d;
    typedef Vec2<std::int32_t> Vec2i;
    typedef Vec2<std::uint32_t> Vec2u;
    typedef Vec2<std::int64_t> Vec2l;
    typedef Vec2<std::uint64_t> Vec2ul;

}
