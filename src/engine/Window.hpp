#pragma once

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <thread>

#include "Stringable.hpp"
#include "math/Vec2.hpp"
#include "color/Color3.hpp"
#include "color/Color4.hpp"
#include "log/Logger.hpp"
#include "engine/scene/gameObject/CameraObject.hpp"



namespace tge {

class Window : Stringable {

public:
    // Get the position of the window on the screen
    Vec2i getPosition() const;

    // Get the size of the window
    Vec2u getSize() const;

    // Get the window title
    std::string getTitle() const;

    // Get active camera object
    std::shared_ptr<CameraObject> getActiveCamera() const;

    // Set the position of the window on the screen.
    void setPosition(const Vec2i& position);

    // Set the size of the window.
    void setSize(const Vec2u& size);

    // Set the window title.
    void setTitle(const std::string& title);

    // Enable/disable vertical sync: lock the framerate to the refresh rate of the monitor (not always 60 fps!)
    void setVSync(bool vSync);

    // Set framerate limit (0 to disable)
    void setFpsLimit(std::uint32_t fpsLimit);

    // Set active camera object
    void setActiveCamera(std::shared_ptr<CameraObject> camera);

    // Update the view to the current active camera
    void setCameraView();


    // Set the renderer callback.
    // The renderer callback will get called on every frame.
    void setRenderer(void (*callback)(Window* win));


    // Opens the window, and starts a separate thread which calls the renderer callback every frame
    void open();


    // Clear the window contents with a specific color.
    void clear(Color3 clearColor = Color3::BLACK);

    // Draw rectangle
    void drawRectangleTest(std::int32_t x, std::int32_t y, std::uint32_t w, std::uint32_t h, Color4 outlineColor, Color4 fillColor = Color4::TRANSPARENT);


    // Returns a reference to the internal sf::RenderWindow
    sf::RenderWindow& getSfWindow() const;


    // Get the string representation of the window
    std::string toString() const;


    // Create a new window
    Window(Vec2u size = Vec2u(800, 600), std::string title = "Game Engine");

    // Destruct the window
    ~Window();

protected:
    void (*rendererCallback)(Window* win) = nullptr;

    sf::RenderWindow* sfWindow;
    Vec2i position;
    Vec2u size;
    std::string title;
    bool vSync;
    std::uint32_t fpsLimit;
    std::shared_ptr<CameraObject> activeCamera;

    std::thread renderThread;

private:
    static void renderThreadHandler(Window* self);

    Logger internalRendererLogger = Logger("tge/render");

};

}
