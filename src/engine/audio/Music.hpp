#pragma once

#include <string>
#include <filesystem>

#include "Playable.hpp"



namespace tge {

class Music : public Playable {

public:
    // Returns true if the sound is ready to play
    // [TODO]
    bool isReady();

    // [TODO]
    void loadFromFile(std::filesystem::path path);
    void loadFromFile(std::string path);

};

}