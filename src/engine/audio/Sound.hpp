#pragma once

#include <string>
#include <filesystem>

#include "Playable.hpp"



namespace tge {

class Sound : public Playable {

public:
    // Returns true if the sound is ready to play
    // [TODO]
    bool isReady();

    // [TODO]
    void loadFromFile(std::filesystem::path path, void (*loadedCallback)() = nullptr);
    void loadFromFile(std::string path, void (*loadedCallback)() = nullptr);
    void loadFromFileSync(std::filesystem::path path);
    void loadFromFileSync(std::string path);

};

}