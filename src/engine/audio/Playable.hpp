#pragma once

#include <chrono>



namespace tge {

class Playable {

public:
    // Returns true if the sound is currently playing.
    virtual bool isPlaying() = 0;

    // Returns the time since the beginning of the sound.
    virtual std::chrono::steady_clock::duration getTime() = 0;

    // Returns the time since the beginning of the sound (in milliseconds).
    std::int64_t getTimeMs();

    // Returns the duration of the sound.
    virtual std::chrono::steady_clock::duration getDuration() = 0;

    // Returns the duration of the sound (in milliseconds).
    std::uint64_t getDurationMs();

    // Returns true if the sound should loop.
    bool isLooping();

    // Returns the duration of the sound.
    std::chrono::steady_clock::duration getLoopPoint();

    // Returns the duration of the sound (in milliseconds).
    std::int64_t getLoopPointMs();

    // Returns the volume of the sound (value from 0.0 to 1.0)
    double getVolume();

    // [TODO]
    double getPitch();

    // [TODO]
    double getSpeed();


    // Start playing from the current point.
    virtual void play() = 0;

    // Stop playing and seek to the beginning.
    virtual void stop();

    // Stop playing.
    // Use play() to unpause.
    virtual void pause() = 0;

    // Change current playing position.
    virtual void seek(std::chrono::steady_clock::duration time) = 0;

    // Change current playing position (in milliseconds).
    void seek(std::int64_t ms);

    // Set to true if the sound should loop
    // Default: false
    virtual void setLoop(bool loop);

    // Set loop point.
    // Default: std::chrono::steady_clock::duration::zero()
    virtual void setLoopPoint(std::chrono::steady_clock::duration time);

    // Change current playing position (in milliseconds).
    // Default: 0
    void setLoopPoint(std::int64_t ms);

    // Set volume (value from 0.0 to 1.0).
    // Default: 1.0
    virtual void setVolume(double volume);

    // Change pitch and sound speed.
    // Default: 1.0
    // [TODO]
    virtual void setPitch(double pitch);

    // Change sound pitch without changing speed.
    // Default: 1.0
    // [TODO]
    virtual void setPitchOnly(double speed);

    // Change sound speed without changing pitch.
    // Default: 1.0
    // [TODO]
    virtual void setSpeedOnly(double speed);

protected:
    bool looping = false;
    std::chrono::steady_clock::duration loopPoint = std::chrono::steady_clock::duration::zero();
    double volume = 1.0;

};

}
