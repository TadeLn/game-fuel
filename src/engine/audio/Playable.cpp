#include "Playable.hpp"



namespace tge {

std::int64_t Playable::getTimeMs() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(this->getTime()).count();
}

std::uint64_t Playable::getDurationMs() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(this->getDuration()).count();
}

bool Playable::isLooping() {
    return this->looping;
}

std::chrono::steady_clock::duration Playable::getLoopPoint() {
    return this->loopPoint;
}

std::int64_t Playable::getLoopPointMs() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(this->getLoopPoint()).count();
}

double Playable::getVolume() {
    return this->volume;
}

void Playable::stop() {
    this->pause();
    this->seek(std::chrono::steady_clock::duration::zero());
}

void Playable::seek(std::int64_t ms) {
    this->seek(std::chrono::milliseconds(ms));
}

void Playable::setLoop(bool loop) {
    this->looping = loop;
}

void Playable::setLoopPoint(std::chrono::steady_clock::duration time) {
    this->loopPoint = time;
}

void Playable::setLoopPoint(std::int64_t ms) {
    this->setLoopPoint(std::chrono::milliseconds(ms));
}

void Playable::setVolume(double volume) {
    this->volume = volume;
}

}
