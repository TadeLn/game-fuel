#pragma once

#include <cstdint>

#include "Stringable.hpp"



namespace tge {

class UUID : public Stringable {

public:
    std::uint64_t i0;
    std::uint64_t i1;

    bool operator<(const UUID uuid) const;
    bool operator==(const UUID uuid) const;

    // [TODO]
    virtual std::string toString() const;

    UUID();

    static UUID create();

private:
    static std::uint64_t tempGlobalCounter;

};

}
