#include "UUID.hpp"

#include <chrono>



namespace tge {

bool UUID::operator<(const UUID uuid) const {
    return this->i1 == uuid.i1 ? (this->i0 < uuid.i0) : (this->i1 < uuid.i1);
}

bool UUID::operator==(const UUID uuid) const {
    return (this->i0 == uuid.i0) && (this->i1 == uuid.i1);
}



std::string UUID::toString() const {
    return "<UUID: " + std::to_string(i1) + ":" + std::to_string(i0) + ">";
}



UUID::UUID() {
    this->i0 = 0;
    this->i1 = 0;
}

UUID UUID::create() {
    UUID uuid;
    uuid.i0 = tempGlobalCounter;
    uuid.i1 = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
    return uuid;
}

std::uint64_t UUID::tempGlobalCounter = 0;

}
