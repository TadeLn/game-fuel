#pragma once

#include <vector>
#include <stdexcept>

#include "TileID.hpp"
#include "math/Vec2.hpp"



namespace tge {

template<typename TileIDType>
class TileMap2D {

public:

    std::size_t getIndexFromCoordinates(std::int32_t x, std::int32_t y) const {
        return (y * width) + x;
    }

    Vec2i getCoordinatesFromIndex(std::size_t index) const {
        return Vec2i(index % width, index / width);
    }

    inline bool isOutOfBounds(std::int32_t x, std::int32_t y) const {
        return x < 0 || std::uint32_t(x) >= width || y < 0 || std::uint32_t(y) >= height;
    }



    std::vector<TileIDType>& getTileMap() {
        return this->tileMap;
    }

    const std::vector<TileIDType>& getTileMapConst() const {
        return this->tileMap;
    }

    void setTileMap(const std::vector<TileIDType>& tileMap, std::uint32_t width, std::uint32_t height, TileIDType outOfBoundsTile = 0){
        this->width = width;
        this->height = height;
        this->outOfBoundsTile = outOfBoundsTile;

        std::uint32_t tileCount = width * height;
        this->tileMap = std::vector<TileIDType>(tileCount);
        
        std::size_t elementsToCopy = tileMap.size() < tileCount ? tileMap.size() : tileCount;
        
        std::copy(
            tileMap.begin(),
            tileMap.begin() + elementsToCopy,
            this->tileMap.begin()
        );
        std::fill(
            this->tileMap.begin() + elementsToCopy,
            this->tileMap.end(),
            this->outOfBoundsTile
        );
    }



    TileIDType getTile(std::int32_t x, std::int32_t y) const {
        if (isOutOfBounds()) {
            throw std::runtime_error("setTile(): getting tile out of bounds");
        }
        return tileMap.at(getIndexFromCoordinates(x, y));
    }

    TileIDType getTileOrOutOfBounds(std::int32_t x, std::int32_t y) const {
        if (isOutOfBounds(x, y)) {
            return outOfBoundsTile;
        }
        return tileMap.at(getIndexFromCoordinates(x, y));
    }

    void setTile(std::int32_t x, std::int32_t y, TileIDType tile) {
        if (isOutOfBounds(x, y)) {
            throw std::runtime_error("setTile(): setting tile out of bounds");
        }
        std::size_t index = getIndexFromCoordinates(x, y);
        this->tileMap[index] = tile;
    }

    void trySetTile(std::int32_t x, std::int32_t y, TileIDType tile) {
        if (isOutOfBounds(x, y)) {
            return;
        }
        std::size_t index = getIndexFromCoordinates(x, y);
        this->tileMap[index] = tile;
    }



    std::uint32_t getWidth() const {
        return this->width;
    }
    std::uint32_t getHeight() const {
        return this->height;
    }



    TileIDType getOutOfBoundsTile() const {
        return this->outOfBoundsTile;
    }

    void setOutOfBoundsTile(TileIDType tileId) {
        this->outOfBoundsTile = tileId;
    }



    // [TODO] resize level, trim unnesecary parts, fill empty space with oobtile
    void resize(std::uint32_t width, std::uint32_t height) {
        this->width = width;
        this->height = height;
        this->tileMap.resize(width * height);
    }

    TileMap2D() {
        this->width = 0;
        this->height = 0;
    }

    TileMap2D(std::uint32_t width, std::uint32_t height) {
        this->width = width;
        this->height = height;
        this->tileMap.resize(width * height);
    }

protected:
    std::vector<TileIDType> tileMap;
    std::uint32_t width;
    std::uint32_t height;
    TileIDType outOfBoundsTile;

};

}
