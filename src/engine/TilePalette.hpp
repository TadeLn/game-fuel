#pragma once

#include <map>

#include "engine/log/Logger.hpp"
#include "engine/String.hpp"
#include "engine/texture/TextureAtlas.hpp"



namespace tge {

template<typename TileIDType>
class TilePalette {

public:
    void assign(TileIDType tileId, std::string textureId) {
        this->tilePalette[tileId] = textureId;
    }

    std::string getTextureId(TileIDType tileId) {
        auto it = this->tilePalette.find(tileId);
        if (it != this->tilePalette.end()) {
            return it->second;

        } else {
            Logger::_internalEngineLogger.error(String::str("Invalid tile ID! (", tileId, ")"), EXCTX);
            return "default";
        }
    }

    TextureFragment getFragment(TileIDType tileId) {
        return this->textureAtlas->getFragment(this->getTextureId(tileId));
    }

    

    TilePalette() = default;

    TilePalette(std::shared_ptr<TextureAtlas> textureAtlas) {
        this->textureAtlas = textureAtlas;
    }

protected:
    std::map<TileIDType, std::string> tilePalette;
    std::shared_ptr<TextureAtlas> textureAtlas;

};

}
