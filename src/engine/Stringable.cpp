#include "Stringable.hpp"



namespace tge {

Stringable::operator std::string() const {
    return this->toString();
}

std::ostream& operator<<(std::ostream& stream, const Stringable& stringable) {
    stream << stringable.toString();
    return stream;
}

}
