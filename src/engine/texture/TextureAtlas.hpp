#pragma once

#include <filesystem>
#include <string>
#include <optional>

#include "Texture.hpp"
#include "TextureFragment.hpp"
#include "../math/Rect.hpp"



namespace tge {

class TextureAtlas {

public:
    void loadFromFile(std::filesystem::path path);

    std::shared_ptr<Texture> getTexture();
    std::optional<TextureFragment> getFragmentO(std::string textureFragmentName, std::uint32_t animationFrame = 0) const;
    TextureFragment getFragmentEx(std::string textureFragmentName, std::uint32_t animationFrame = 0) const;
    TextureFragment getFragment(std::string textureFragmentName, std::uint32_t animationFrame = 0) const;

    TextureAtlas() = default;
    TextureAtlas(std::filesystem::path path);

private:
    std::shared_ptr<Texture> texture;
    std::map<std::pair<std::string, std::uint32_t>, Recti> textureFragments;

};

}
