#pragma once


#include <filesystem>

#include <SFML/Graphics.hpp>



namespace tge {

class Texture {

public:
    sf::Texture& getSfTexture();

    void loadTexture(std::filesystem::path filename);

    Texture() = default;
    Texture(std::filesystem::path filename);
    Texture(sf::Texture texture);

private:
    sf::Texture sfTexture;

};

}