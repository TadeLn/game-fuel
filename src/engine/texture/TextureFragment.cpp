#include "TextureFragment.hpp"



namespace tge {

std::shared_ptr<Texture> TextureFragment::getTexture() const {
    return this->texture;
}

Recti TextureFragment::getRect() const {
    return this->rect;
}

void TextureFragment::applyToSprite(sf::Sprite& sprite) const {
    sprite.setTexture(this->texture->getSfTexture());
    sprite.setTextureRect(sf::IntRect(rect.x, rect.y, rect.w, rect.h));
}

void TextureFragment::applyToShape(sf::Shape& shape) const {
    shape.setTexture(&this->texture->getSfTexture());
    shape.setTextureRect(sf::IntRect(rect.x, rect.y, rect.w, rect.h));
}

TextureFragment::TextureFragment(std::shared_ptr<Texture> texture, Recti rect) {
    this->texture = texture;
    this->rect = rect;
}

}