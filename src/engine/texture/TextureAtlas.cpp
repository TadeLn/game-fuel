#include "TextureAtlas.hpp"

#include "engine/json/Array.hpp"
#include "engine/json/Object.hpp"
#include "engine/exception/InvalidArgumentException.hpp"
#include "engine/String.hpp"

#include <iostream>



namespace tge {

void TextureAtlas::loadFromFile(std::filesystem::path path) {
    this->texture = std::make_shared<Texture>(path);
    this->textureFragments = std::map<std::pair<std::string, std::uint32_t>, Recti>{};

    std::string atlasFilename = path.string() + ".atlas";

    auto root = json::Node::fromFileEx(atlasFilename);
    auto fragments = root->getPropertyEx("fragments")->cast<json::Array>();
    json::array fragmentsArr = fragments->getEx<json::array>();

    for (std::shared_ptr<json::Node> element : fragmentsArr) {
        std::string name = element->getPropertyEx("name")->getEx<std::string>();
        std::uint32_t frame = element->getProperty("frame")->getOr<std::uint32_t>(0);
        json::Array* rect = element->getPropertyEx("rect")->cast<json::Array>();
        std::int32_t x = rect->getElementEx(0)->getEx<std::int32_t>();
        std::int32_t y = rect->getElementEx(1)->getEx<std::int32_t>();
        std::uint32_t w = rect->getElementEx(2)->getEx<std::uint32_t>();
        std::uint32_t h = rect->getElementEx(3)->getEx<std::uint32_t>();

        this->textureFragments[std::pair(name, frame)] = Recti(x, y, w, h);
    }
}



std::shared_ptr<Texture> TextureAtlas::getTexture() {
    return this->texture;
}

std::optional<TextureFragment> TextureAtlas::getFragmentO(std::string textureFragmentName, std::uint32_t animationFrame) const {
    auto it = this->textureFragments.find(std::pair(textureFragmentName, animationFrame));
    if (it != this->textureFragments.end()) {
        return std::optional(TextureFragment(this->texture, it->second));
    } else {
        return std::optional<TextureFragment>();
    }
}

TextureFragment TextureAtlas::getFragmentEx(std::string textureFragmentName, std::uint32_t animationFrame) const {
    auto fragment = this->getFragmentO(textureFragmentName, animationFrame);
    if (fragment.has_value()) {
        return fragment.value();
    } else {
        throw InvalidArgumentException("textureFragmentName", "invalid id and/or animation frame: \"" + textureFragmentName + "\", frame " + std::to_string(animationFrame), EXCTX);
    }
}

TextureFragment TextureAtlas::getFragment(std::string textureFragmentName, std::uint32_t animationFrame) const {
    auto fragment = this->getFragmentO(textureFragmentName, animationFrame);
    if (fragment.has_value()) {
        return fragment.value();
    } else {
        Logger::_internalEngineLogger.error(String::str("Invalid texture fragment! \"", textureFragmentName, "\", frame ", animationFrame), EXCTX);
        return this->getFragmentEx("default", 0);
    }
}



TextureAtlas::TextureAtlas(std::filesystem::path path) {
    this->loadFromFile(path);
}

}
