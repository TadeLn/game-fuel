#include "Texture.hpp"



namespace tge {

sf::Texture& Texture::getSfTexture() {
    return this->sfTexture;
}

void Texture::loadTexture(std::filesystem::path filename) {
    this->sfTexture.loadFromFile(filename);
}

Texture::Texture(std::filesystem::path filename) {
    this->loadTexture(filename);
}

Texture::Texture(sf::Texture texture) {
    this->sfTexture = texture;
}

}
