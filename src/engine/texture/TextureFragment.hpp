#pragma once

#include <memory>

#include "../math/Rect.hpp"
#include "Texture.hpp"



namespace tge {

class TextureFragment {

public:
    std::shared_ptr<Texture> getTexture() const;
    Recti getRect() const;

    void applyToSprite(sf::Sprite& sprite) const;
    void applyToShape(sf::Shape& shape) const;

    TextureFragment() = default;
    TextureFragment(std::shared_ptr<Texture> texture, Recti rect);

protected:
    std::shared_ptr<Texture> texture;
    Recti rect;

};

}
