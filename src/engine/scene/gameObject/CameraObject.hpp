#pragma once

#include <SFML/Graphics.hpp>

#include "./PositionedObject.hpp"
#include "../../math/Rect.hpp"



namespace tge {

class CameraObject : public PositionedObject {

public:
    enum class Direction {
        NONE,
        HORIZONTAL,
        VERTICAL
    };

    double getViewWidth() const;
    double getViewHeight() const;

    Rectd getViewRectangle() const;
    sf::View getSfView() const;

    void setViewWidth(double width);
    void setViewHeight(double height);
    void setViewLock(Direction viewLock, double viewLockSize);

    void updateCamera(double windowWidth, double windowHeight);

    CameraObject() = default;
    CameraObject(double width, double height, Direction viewLock);


protected:
    Direction viewLock = Direction::NONE;
    Vec2d viewSize = Vec2d(100, 100);

};

}
