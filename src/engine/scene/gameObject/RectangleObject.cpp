#include "RectangleObject.hpp"



namespace tge {

Vec2d RectangleObject::getSize() const {
    return this->size;
}

Rectd RectangleObject::getRect() const {
    return Rectd(this->position, this->size);
}

void RectangleObject::setSize(Vec2d size) {
    this->size = size;
}

void RectangleObject::setRect(Rectd rect) {
    this->position = rect.getPosition();
    this->size = rect.getSize();
}

void RectangleObject::grow(Vec2d sizeChangeVector) {
    this->size += sizeChangeVector;
}

void RectangleObject::growFromCenter(Vec2d sizeChangeVector) {
    this->position -= (sizeChangeVector / 2.0);
    this->size += sizeChangeVector;
}

RectangleObject::RectangleObject(std::string name) : PositionedObject(name) {}

}
