#pragma once

#include "./GameObject.hpp"
#include "../../math/Vec2.hpp"



namespace tge {

class PositionedObject : public GameObject {

public:
    // Get the object's position
    Vec2d getPosition() const;

    // Set the object's position
    void setPosition(Vec2d position);

    // Change the object's position by a vector
    void move(Vec2d moveVector);


protected:
    Vec2d position;

    PositionedObject(std::string name);

};

}
