#pragma once

#include "GameObject.hpp"
#include "engine/TileID.hpp"
#include "engine/TileMap2D.hpp"
#include "engine/TilePalette.hpp"

namespace tge
{

    class TileRendererObject : public GameObject
    {

    public:
        void onFrame(Scene *scene, Window &win, double deltaTime);

        const TileMap2D<TileID> getTileMap() const;
        void setTileMap(TileMap2D<TileID> tileMap);
        void setTilePalette(TilePalette<TileID> tilePalette);

        TileRendererObject(TileMap2D<TileID> tileMap, TilePalette<TileID> tilePalette);

        bool debugMode = false;

        const static std::uint32_t TILE_WIDTH = 32;
        const static std::uint32_t TILE_HEIGHT = 32;

    protected:
        TileMap2D<TileID> tileMap;
        TilePalette<TileID> tilePalette;

    private:
        void renderDebug(Window &win);
        void render(Window &win);
    };

}
