#pragma once

#include <optional>

namespace tge {
    class Scene;
}

#include "engine/Stringable.hpp"
#include "engine/UUID.hpp"



namespace tge {

class Window;

class GameObject : public Stringable {

public:
    // Get the object's name.
    std::string getName() const;


    // Get the object's unique identifier.
    UUID getUUID() const;


    // This method is called right after the object was added to the scene.
    // You may override this method to implement your own initialization functionality.
    virtual void onCreate(Scene* scene);

    // This method is called when the object is about to be removed from the scene.
    // You may override this method to implement your own application close functionality.
    virtual void onDestroy(Scene* scene);

    // This method is called on the main thread on every tick.
    virtual void onTick(Scene* scene);

    // This method is called on the render thread, when the frame needs to be rendered.
    // deltaTime is the amount of time that has passed since the last time this function was called, in milliseconds
    virtual void onFrame(Scene* scene, Window& win, double deltaTime);


    // Get the string representation of the object
    // [TODO]
    virtual std::string toString() const;

protected:
    GameObject(std::string name);

    std::string name = "GameObject";

private:
    UUID uuid;

};

}
