#pragma once

#include "RectangleObject.hpp"
#include "../../texture/TextureFragment.hpp"
#include "../../math/Rect.hpp"



namespace tge {

class SpriteObject : public RectangleObject {

public:
    void onFrame(Scene* scene, Window& win, double deltaTime);
    
    virtual const TextureFragment& getTextureFragment() const;
    void setTextureFragment(TextureFragment textureFragment);

    SpriteObject();
    SpriteObject(TextureFragment textureFragment);

protected:
    TextureFragment textureFragment;

};

}
