#include "TileRendererObject.hpp"

#include <cmath>

#include "engine/color/Color4.hpp"
#include "engine/Window.hpp"
#include "engine/texture/TextureFragment.hpp"



namespace tge {

void TileRendererObject::onFrame(__attribute__ ((unused)) Scene* scene, Window& win, __attribute__ ((unused)) double deltaTime) {
    if (this->debugMode) {
        renderDebug(win);
    } else {
        render(win);
    }
}



const TileMap2D<TileID> TileRendererObject::getTileMap() const {
    return tileMap;
}

void TileRendererObject::setTileMap(TileMap2D<TileID> tileMap) {
    this->tileMap = tileMap;
}

void TileRendererObject::setTilePalette(TilePalette<TileID> tilePalette) {
    this->tilePalette = tilePalette;
}



TileRendererObject::TileRendererObject(TileMap2D<TileID> tileMap, TilePalette<TileID> tilePalette) : GameObject("TileRenderer") {
    setTileMap(tileMap);
    setTilePalette(tilePalette);
}



void TileRendererObject::renderDebug(Window& win) {
    this->render(win);

    const std::uint32_t width = this->tileMap.getWidth();
    const std::uint32_t height = this->tileMap.getHeight();

    std::shared_ptr<CameraObject> camera = win.getActiveCamera();
    Rectd cameraRectangle = camera->getViewRectangle();

    std::int32_t tileXBegin = floor(cameraRectangle.x / TILE_WIDTH);
    std::int32_t tileYBegin = floor(cameraRectangle.y / TILE_HEIGHT);
    std::int32_t tileXCount = floor(cameraRectangle.w / TILE_WIDTH) + 2;
    std::int32_t tileYCount = floor(cameraRectangle.h / TILE_HEIGHT) + 2;

    for (std::int32_t y = tileYBegin; y < tileYBegin + tileYCount; y++) {
        if (y < 0) continue;
        if (y >= (std::int32_t) height) break;

        for (std::int32_t x = tileXBegin; x < tileXBegin + tileXCount; x++) {
            if (x < 0) continue;
            if (x >= (std::int32_t) width) break;
            
            TileID tileId = this->tileMap.getTileOrOutOfBounds(x, y);
            Color4 outlineColor = tileId == 0 ? Color4::TRANSPARENT : Color4::GREEN;
            Color4 fillColor = outlineColor * Color4::TRANSPARENT;
            win.drawRectangleTest(x * TILE_WIDTH, y * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT, outlineColor, fillColor);
        }
    }
}

void TileRendererObject::render(__attribute__ ((unused)) Window& win) {
    const std::uint32_t width = this->tileMap.getWidth();
    const std::uint32_t height = this->tileMap.getHeight();

    std::shared_ptr<CameraObject> camera = win.getActiveCamera();
    Rectd cameraRectangle = camera->getViewRectangle();

    std::int32_t tileXBegin = floor(cameraRectangle.x / TILE_WIDTH);
    std::int32_t tileYBegin = floor(cameraRectangle.y / TILE_HEIGHT);
    std::int32_t tileXCount = floor(cameraRectangle.w / TILE_WIDTH) + 2;
    std::int32_t tileYCount = floor(cameraRectangle.h / TILE_HEIGHT) + 2;

    for (std::int32_t y = tileYBegin; y < tileYBegin + tileYCount; y++) {
        if (y < 0) continue;
        if (y >= (std::int32_t) height) break;

        for (std::int32_t x = tileXBegin; x < tileXBegin + tileXCount; x++) {
            if (x < 0) continue;
            if (x >= (std::int32_t) width) break;
            
            TileID tileId = this->tileMap.getTileOrOutOfBounds(x, y);
            TextureFragment textureFragment = this->tilePalette.getFragment(tileId);
            sf::RenderWindow& window = win.getSfWindow();
            sf::RectangleShape tileShape;
            tileShape.setPosition(x * TILE_WIDTH, y * TILE_HEIGHT);
            tileShape.setSize(sf::Vector2f(TILE_WIDTH, TILE_HEIGHT));
            textureFragment.applyToShape(tileShape);
            window.draw(tileShape);
        }
    }
}

}
