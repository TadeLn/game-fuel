#include "PositionedObject.hpp"



namespace tge {

// Get the object's position
Vec2d PositionedObject::getPosition() const {
    return this->position;
}

// Set the object's position
void PositionedObject::setPosition(Vec2d position) {
    this->position = position;
}

// Change the object's position by a vector
void PositionedObject::move(Vec2d moveVector) {
    setPosition(this->position + moveVector);
}

PositionedObject::PositionedObject(std::string name) : GameObject(name) {}

}
