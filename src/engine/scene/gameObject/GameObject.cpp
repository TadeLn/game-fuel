#include "GameObject.hpp"



namespace tge {

std::string GameObject::getName() const {
    return this->name;
}

UUID GameObject::getUUID() const {
    return this->uuid;
}

void GameObject::onCreate(__attribute__ ((unused)) Scene* scene) {

}

void GameObject::onDestroy(__attribute__ ((unused)) Scene* scene) {

}

void GameObject::onTick(__attribute__ ((unused)) Scene* scene) {

}

void GameObject::onFrame(__attribute__ ((unused)) Scene* scene, __attribute__ ((unused)) Window& win, __attribute__ ((unused)) double deltaTime) {

}

std::string GameObject::toString() const {
    return "<GameObject: \"" + this->name + "\">";
}

GameObject::GameObject(std::string name) {
    this->name = name;
    this->uuid = UUID::create();
}

}
