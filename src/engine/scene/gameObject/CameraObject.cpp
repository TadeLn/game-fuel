#include "CameraObject.hpp"


namespace tge {
    
double CameraObject::getViewWidth() const {
    return this->viewSize.x;
}

double CameraObject::getViewHeight() const {
    return this->viewSize.y;
}



Rectd CameraObject::getViewRectangle() const {
    return Rectd(this->getPosition() - (viewSize / 2), viewSize);
}

sf::View CameraObject::getSfView() const {
    Rectd rectangle = this->getViewRectangle();
    return sf::View(sf::FloatRect(rectangle.x, rectangle.y, rectangle.w, rectangle.h)); 
}



void CameraObject::setViewWidth(double width) {
    this->viewSize.x = width;
}

void CameraObject::setViewHeight(double height) {
    this->viewSize.y = height;
}

void CameraObject::setViewLock(Direction viewLock, double viewLockSize) {
    this->viewLock = viewLock;

    if (viewLock == Direction::HORIZONTAL) {
        this->viewSize.x = viewLockSize;

    } else if (viewLock == Direction::VERTICAL) {
        this->viewSize.y = viewLockSize;

    }
}



void CameraObject::updateCamera(double windowWidth, double windowHeight) {
    if (viewLock == Direction::HORIZONTAL) {
        this->viewSize.y = (windowHeight / windowWidth) * this->viewSize.x;

    } else if (viewLock == Direction::VERTICAL) {
        this->viewSize.x = (windowWidth / windowHeight) * this->viewSize.y;

    } else {
        this->viewSize = Vec2d(windowWidth, windowHeight);
    }
}

CameraObject::CameraObject(double width, double height, Direction viewLock) : PositionedObject("Camera") {
    this->viewSize = Vec2d(width, height);
    this->viewLock = viewLock;
}

}
