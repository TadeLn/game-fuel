#pragma once

#include "./PositionedObject.hpp"
#include "../../math/Rect.hpp"



namespace tge {

class RectangleObject : public PositionedObject {

public:
    // Get the object's size
    Vec2d getSize() const;

    // Get the object's position and size
    Rectd getRect() const;

    // Set the object's size
    void setSize(Vec2d position);

    // Set the object's position and size
    void setRect(Rectd rect);

    // Increase or decrease the size of the object
    void grow(Vec2d sizeChangeVector);

    // Increase or decrease the size of the object, and adjust the position vector so that the object's center stays in the same place
    void growFromCenter(Vec2d sizeChangeVector);


protected:
    Vec2d size;

    RectangleObject(std::string name);

};

}
