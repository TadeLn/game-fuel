#include "SpriteObject.hpp"

#include "engine/Window.hpp"



namespace tge {

void SpriteObject::onFrame(__attribute__ ((unused)) Scene* scene, Window& win, __attribute__ ((unused)) double deltaTime) {
    sf::RenderWindow& sfWin = win.getSfWindow();
    const TextureFragment& textureFragment = getTextureFragment();
    sf::Sprite sprite;
    sprite.setPosition(this->getPosition());
    textureFragment.applyToSprite(sprite);
    sfWin.draw(sprite);
}

const TextureFragment& SpriteObject::getTextureFragment() const {
    return this->textureFragment;
}

void SpriteObject::setTextureFragment(TextureFragment textureFragment) {
    this->textureFragment = textureFragment;
}

SpriteObject::SpriteObject() : RectangleObject("Sprite") {}

SpriteObject::SpriteObject(TextureFragment textureFragment) : SpriteObject() {
    this->textureFragment = textureFragment;
}

}
