#pragma once

#include <memory>
#include <map>

#include "../UUID.hpp"
#include "../Stringable.hpp"
#include "gameObject/GameObject.hpp"



namespace tge {

class Scene : public Stringable {

public:
    // Get the scene's name
    // [TODO]
    std::string getName() const;



    // Get map of objects
    const std::map<UUID, std::shared_ptr<GameObject>>& getObjects() const;

    // Get an object frome the scene
    std::weak_ptr<GameObject> getObject(UUID objectUUID) const;

    // Add an object to the scene
    UUID addObject(std::unique_ptr<GameObject> object);

    // Remove an object frome the scene
    void removeObject(UUID objectUUID);


    void onFrame(Window& win, double deltaTime);


    // Get the string representation of the object
    std::string toString() const;


    // Create a new scene
    Scene(std::string name);

protected:
    std::string name;

    std::map<UUID, std::shared_ptr<GameObject>> objects;

};

}
