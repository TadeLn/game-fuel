#include "Scene.hpp"

#include "../exception/InvalidOperationException.hpp"

namespace tge
{

    const std::map<UUID, std::shared_ptr<GameObject>> &Scene::getObjects() const
    {
        return objects;
    }

    std::weak_ptr<GameObject> Scene::getObject(UUID objectUUID) const
    {
        auto it = objects.find(objectUUID);
        if (it == objects.end())
        {
            throw tge::InvalidOperationException("no object with the given uuid was found", EXCTX);
        }
        return it->second;
    }

    UUID Scene::addObject(std::unique_ptr<GameObject> object)
    {
        UUID objectUUID = object->getUUID();
        object->onCreate(this);
        this->objects[objectUUID] = std::shared_ptr(std::move(object));
        return objectUUID;
    }

    void Scene::removeObject(UUID objectUUID)
    {
        auto it = this->objects.find(objectUUID);
        if (it != this->objects.end())
        {
            it->second->onDestroy(this);
        }

        if (this->objects.erase(objectUUID) == 0)
        {
            throw tge::InvalidOperationException("cannot remove object with the uuid " + objectUUID.toString() + " because the object is not present in the scene", EXCTX);
        }
    }

    void Scene::onFrame(Window &win, double deltaTime)
    {
        for (auto object : objects)
        {
            object.second->onFrame(this, win, deltaTime);
        }
    }

    std::string Scene::toString() const
    {
        return "<Scene: \"" + this->name + "\">";
    }

    Scene::Scene(std::string name)
    {
        this->name = name;
    }

}
