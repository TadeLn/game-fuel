#include "UTF8.hpp"

#include "exception/InvalidArgumentException.hpp"


namespace tge {


std::string UTF8::encodeUTF8(std::uint32_t unicodeCodepoint) {
    if (unicodeCodepoint > 0x10ffff) {
        throw InvalidArgumentException("unicodeCodepouint", "unicode codepoint cannot be larger than 0x10ffff", EXCTX);
    }

    if (unicodeCodepoint < (1 << 7)) {
        return std::string(1, (char) unicodeCodepoint);
    }
    if (unicodeCodepoint < (1 << 11)) {
        return
            std::string(1, (char) ((0b11000000) | ((unicodeCodepoint >> 6) & 0b00011111))) +
            std::string(1, (char) ((0b10000000) | ((unicodeCodepoint     ) & 0b00111111)));
    }
    if (unicodeCodepoint < (1 << 16)) {
        return
            std::string(1, (char) ((0b11100000) | ((unicodeCodepoint >> 12) & 0b00001111))) +
            std::string(1, (char) ((0b10000000) | ((unicodeCodepoint >> 6 ) & 0b00111111))) +
            std::string(1, (char) ((0b10000000) | ((unicodeCodepoint      ) & 0b00111111)));
    }
    if (unicodeCodepoint < (1 << 21)) {
        return
            std::string(1, (char) ((0b11110000) | ((unicodeCodepoint >> 18) & 0b00000111))) +
            std::string(1, (char) ((0b10000000) | ((unicodeCodepoint >> 12) & 0b00111111))) +
            std::string(1, (char) ((0b10000000) | ((unicodeCodepoint >> 6 ) & 0b00111111))) +
            std::string(1, (char) ((0b10000000) | ((unicodeCodepoint      ) & 0b00111111)));
    } else {
        return "";
    }
}

std::string UTF8::encodeUTF8(std::vector<std::uint32_t> unicodeCodepoints) {
    std::string result = "";
    for (auto codepoint : unicodeCodepoints) {
        result += encodeUTF8(codepoint);
    }
    return result;
}


UTF8::UTF8DecodeResult UTF8::decodeUTF8(std::string utf8string) {
    UTF8DecodeResult result;
    
    std::uint8_t bytesLeftToRead = 0;
    std::uint32_t currentCodepoint = 0;

    for (auto it = utf8string.begin(); it != utf8string.end(); ++it) {
        if (bytesLeftToRead == 0) {
            if (*it >= 0) {
                // Handle regular ASCII characters
                result.codepoints.push_back(*it);

            } else if ((*it & 0b11100000) == 0b11000000) {
                // Handle 110xxxxx byte (1 more byte to read)
                bytesLeftToRead = 1;
                result.remainingBytes = 2;
                currentCodepoint = (*it) & 0b00011111;

            } else if ((*it & 0b11110000) == 0b11100000) {
                // Handle 1110xxxx byte (2 more bytes to read)
                bytesLeftToRead = 2;
                result.remainingBytes = 3;
                currentCodepoint = (*it) & 0b00001111;
                
            } else if ((*it & 0b11111000) == 0b11110000) {
                // Handle 11110xxx byte (3 more bytes to read)
                bytesLeftToRead = 3;
                result.remainingBytes = 4;
                currentCodepoint = (*it) & 0b00000111;
            }
        } else {
            // Read the 10xxxxxx bytes
            currentCodepoint = (currentCodepoint << 6) | ((*it) & 0b00111111);
        }
    }

    if (bytesLeftToRead == 0) {
        result.remainingBytes = 0;
    }

    return result;
}

}
