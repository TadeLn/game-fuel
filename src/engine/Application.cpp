#include "Application.hpp"

#include <thread>
#include <csignal>
#include <iostream>



namespace tge {

int Application::start(int argc, char** argv) {
    Application::instance = this;

    std::vector<std::string> commandLineArguments;
    commandLineArguments.reserve(argc);

    for (std::uint32_t i = 0; i < (std::uint32_t) argc; i++) {
        commandLineArguments.push_back(argv[i]);
    }


    signal(SIGINT, [](int signalNum) {
        Application::getInstance()->onSignal(signalNum);
    });


    mainWindow = new Window(this->initialWindowSize, this->applicationName);

    running = true;
    mainWindow->open();

    mainWindow->setRenderer(Application::renderHandler);
    
    this->onInit(commandLineArguments, *mainWindow);
    
    while (this->isRunning()) {
        // [TODO] tick timer
        std::this_thread::sleep_for(std::chrono::milliseconds(1));

        this->onTick();
    }

    this->onDestroy(*mainWindow);
    return 0;
}

void Application::stop() {
    this->running = false;
}

bool Application::isRunning() const {
    return this->running;
}

std::uint32_t Application::getFps() const {
    return this->fps;
}

void Application::onInit(__attribute__ ((unused)) const std::vector<std::string>& commandLineArguments, __attribute__ ((unused)) Window& win) {
    
}

void Application::onDestroy(__attribute__ ((unused)) Window& win) {

}

void Application::onTick() {

}

void Application::onFrame(__attribute__ ((unused)) Window& win, __attribute__ ((unused)) double deltaTime) {

}

void Application::onSignal(__attribute__ ((unused)) int signalNum) {

}

void Application::onKeyboard(__attribute__ ((unused)) const KeyboardEvent& event) {

}

void Application::onText(__attribute__ ((unused)) const TextEvent& event) {

}

Application::Application(std::string applicationName, Vec2u windowSize) {
    this->applicationName = applicationName;
    this->initialWindowSize = windowSize;
}

Application* Application::getInstance() {
    return Application::instance;
}

Application* Application::instance = nullptr;

void Application::renderHandler(Window* win) {
    std::chrono::steady_clock::time_point functionStartTime = std::chrono::steady_clock::now();
    Application* self = Application::getInstance();

    if ((functionStartTime - self->lastFpsCheck) > std::chrono::seconds(1)) {
        self->lastFpsCheck = functionStartTime;
        self->fps = self->frameCount;
        self->frameCount = 0;
    }

    double deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(functionStartTime - self->lastRender).count() / 1000.0;

    win->clear();
    win->setCameraView();
    if (self->activeScene != nullptr) {
        self->activeScene->onFrame(*win, deltaTime);
    }
    self->onFrame(*win, deltaTime);
    
    self->lastRender = functionStartTime;
    self->frameCount++;
}

}
