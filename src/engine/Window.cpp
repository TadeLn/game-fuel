#include "Window.hpp"

#include "Application.hpp"
#include "input/Keyboard.hpp"
#include "input/Mouse.hpp"



namespace tge {

Vec2i Window::getPosition() const {
    if (this->sfWindow->isOpen()) {
        sf::Vector2i position = this->sfWindow->getPosition();
        return Vec2i(position.x, position.y);

    } else {
        return this->position;
    }
}

Vec2u Window::getSize() const {
    if (this->sfWindow->isOpen()) {
        sf::Vector2u size = this->sfWindow->getSize();
        return Vec2u(size.x, size.y);

    } else {
        return this->size;
    }
}

std::string Window::getTitle() const {
    return this->title;
}

std::shared_ptr<CameraObject> Window::getActiveCamera() const {
    return this->activeCamera;
}

void Window::setPosition(const Vec2i& position) {
    this->position = position;
    if (this->sfWindow != nullptr && this->sfWindow->isOpen()) {
        this->sfWindow->setPosition(sf::Vector2i(position.x, position.y));
    }
}

void Window::setSize(const Vec2u& size) {
    this->size = size;
    if (this->sfWindow != nullptr && this->sfWindow->isOpen()) {
        this->sfWindow->setSize(sf::Vector2u(size.x, size.y));
    }
}

void Window::setTitle(const std::string& title) {
    this->title = title;
    if (this->sfWindow != nullptr && this->sfWindow->isOpen()) {
        this->sfWindow->setTitle(title);
    }
}

void Window::setVSync(bool vSync) {
    this->vSync = vSync;
    if (this->sfWindow != nullptr && this->sfWindow->isOpen()) {
        this->sfWindow->setVerticalSyncEnabled(vSync);
    }
}

void Window::setFpsLimit(std::uint32_t fpsLimit) {
    this->fpsLimit = fpsLimit;
    if (this->sfWindow != nullptr && this->sfWindow->isOpen()) {
        this->sfWindow->setFramerateLimit(fpsLimit);
    }
}

void Window::setActiveCamera(std::shared_ptr<CameraObject> camera) {
    this->activeCamera = camera;
    setCameraView();
}

void Window::setCameraView() {
    if (this->sfWindow != nullptr && this->sfWindow->isOpen()) {
        Vec2u windowSize = this->getSize();
        this->activeCamera->updateCamera(windowSize.x, windowSize.y);
        this->sfWindow->setView(this->activeCamera->getSfView());
    }
}

void Window::setRenderer(void (*callback)(Window* win)) {
    this->rendererCallback = callback;
}



void Window::open() {
    this->renderThread = std::thread(Window::renderThreadHandler, this);
}


void Window::clear(Color3 clearColor) {
    this->sfWindow->clear(sf::Color(clearColor.r, clearColor.g, clearColor.b));
}

void Window::drawRectangleTest(std::int32_t x, std::int32_t y, std::uint32_t w, std::uint32_t h, Color4 outlineColor, Color4 fillColor) {
    sf::RectangleShape r;
    r.setPosition(x + 1, y + 1);
    r.setSize(sf::Vector2f(w, h));
    r.setOutlineColor(outlineColor);
    r.setOutlineThickness(1);
    r.setFillColor(fillColor);
    this->sfWindow->draw(r);
}



sf::RenderWindow& Window::getSfWindow() const {
    return *this->sfWindow;
}



std::string Window::toString() const {
    return "<Window: " + this->getTitle() + ">";
}



Window::Window(Vec2u size, std::string title) {
    this->sfWindow = nullptr;
    this->setSize(size);
    this->setTitle(title);
    this->setVSync(true);
    this->setFpsLimit(0);
}

Window::~Window() {
    delete this->sfWindow;
}

void Window::renderThreadHandler(Window* self) {
    self->sfWindow = new sf::RenderWindow(sf::VideoMode(self->size.x, self->size.y), self->getTitle());

    sf::RenderWindow* win = self->sfWindow;
    win->setPosition(sf::Vector2i(self->position.x, self->position.y));
    win->setVerticalSyncEnabled(self->vSync);
    win->setFramerateLimit(self->fpsLimit);

    self->clear();

    while (win->isOpen()) {

        sf::Event event;
        while (win->pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:
                    win->close();
                    Application::getInstance()->stop();
                    break;
                
                case sf::Event::Resized:
                    {
                        sf::View view = win->getView();
                        view.setSize(sf::Vector2f(event.size.width, event.size.height));
                        win->setView(view);
                    }
                    break;

                case sf::Event::LostFocus:
                    Keyboard::releaseAllKeys();
                    break;

                case sf::Event::GainedFocus:
                    break;

                case sf::Event::KeyPressed:
                    Keyboard::changeState(Keyboard::fromSfKey(event.key.code), true);
                    break;

                case sf::Event::KeyReleased:
                    Keyboard::changeState(Keyboard::fromSfKey(event.key.code), false);
                    break;

                case sf::Event::TextEntered:
                    Application::getInstance()->onText(TextEvent(std::chrono::steady_clock::now(), event.text.unicode));
                    break;

                case sf::Event::MouseButtonPressed:
                    Mouse::changeButtonState(true, Mouse::fromSfButton(event.mouseButton.button), Vec2i(event.mouseButton.x, event.mouseButton.y));
                    break;

                case sf::Event::MouseButtonReleased:
                    Mouse::changeButtonState(false, Mouse::fromSfButton(event.mouseButton.button), Vec2i(event.mouseButton.x, event.mouseButton.y));
                    break;

                case sf::Event::MouseEntered:
                    Mouse::changeHoverState(true);
                    break;

                case sf::Event::MouseLeft:
                    Mouse::changeHoverState(false);
                    break;

                case sf::Event::MouseMoved:
                    Mouse::changePositionState(Vec2i(event.mouseMove.x, event.mouseMove.y));
                    break;

                case sf::Event::MouseWheelScrolled:
                    Mouse::changeScrollState(Mouse::fromSfWheel(event.mouseWheelScroll.wheel), event.mouseWheelScroll.delta, Vec2i(event.mouseWheelScroll.x, event.mouseWheelScroll.y));
                    break;

                case sf::Event::MouseWheelMoved:
                    // Deprecated event
                    break;

                default:
                    self->internalRendererLogger.warning(std::string("Unhandled window event; e.type = ") + std::to_string(event.type), EXCTX);
                    break;
            }
        }

        if (self->rendererCallback != nullptr) {
            self->rendererCallback(self);
        }

        win->display();
    }

}

}
