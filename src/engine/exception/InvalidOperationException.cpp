#include "InvalidOperationException.hpp"


namespace tge {

std::string InvalidOperationException::getDescription() const {
    return description;
}


InvalidOperationException::InvalidOperationException(std::string file, std::uint32_t line)
    : Exception("invalid operation", file, line) {}

InvalidOperationException::InvalidOperationException(std::string description, std::string file, std::uint32_t line)
    : Exception("invalid operation: " + description, file, line) {
    this->description = description;
}

}