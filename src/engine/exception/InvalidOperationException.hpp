#pragma once

#include "Exception.hpp"


namespace tge {

class InvalidOperationException : public Exception {

public:
    std::string getDescription() const;
    InvalidOperationException(std::string file, std::uint32_t line);
    InvalidOperationException(std::string description, std::string file, std::uint32_t line);

private:
    std::string description;

};

}
