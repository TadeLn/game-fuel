#pragma once

#include "Exception.hpp"


namespace tge {

class InvalidTypeException : public Exception {

public:
    InvalidTypeException(std::string file, std::uint32_t line);

};

}
