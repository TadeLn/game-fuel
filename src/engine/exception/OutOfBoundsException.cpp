#include "OutOfBoundsException.hpp"



namespace tge {

OutOfBoundsException::OutOfBoundsException(std::string file, std::uint32_t line)
    : Exception("accessing out-of-bounds data", file, line) {}

OutOfBoundsException::OutOfBoundsException(std::string message, std::string file, std::uint32_t line)
    : Exception(message, file, line) {}

}
