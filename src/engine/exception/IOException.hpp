#pragma once

#include <cstdint>

#include "Exception.hpp"


namespace tge {

class IOException : public Exception {

public:
    IOException(std::string file, std::uint32_t line);

protected:
    IOException(std::string message, std::string file, std::uint32_t line);

};

}