#include "IOException.hpp"



namespace tge {

IOException::IOException(std::string file, std::uint32_t line)
    : Exception("input-output operation failed", file, line) {}

IOException::IOException(std::string message, std::string file, std::uint32_t line)
    : Exception(message, file, line) {}

}