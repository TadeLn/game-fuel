#pragma once

#include "Exception.hpp"


namespace tge {

class InvalidArgumentException : public Exception {

public:
    std::string getArgumentName() const;
    std::string getDescription() const;
    InvalidArgumentException(std::string file, std::uint32_t line);
    InvalidArgumentException(std::string argumentName, std::string file, std::uint32_t line);
    InvalidArgumentException(std::string argumentName, std::string description, std::string file, std::uint32_t line);

private:
    std::string argumentName;
    std::string description;

};

}
