#include "WriteOnceException.hpp"



namespace tge {

std::string WriteOnceException::getVariableName() const {
    return variableName;
}

WriteOnceException::WriteOnceException(std::string variableName, std::string file, std::uint32_t line)
    : Exception("write to an already initialized write-once variable \"" + variableName + "\"", file, line) {
    this->variableName = variableName;
}

}