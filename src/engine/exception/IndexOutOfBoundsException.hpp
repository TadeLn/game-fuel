#pragma once

#include "OutOfBoundsException.hpp"


namespace tge {

class IndexOutOfBoundsException : public OutOfBoundsException {

public:
    std::size_t getIndex() const;
    std::size_t getArrayLength() const;
    IndexOutOfBoundsException(std::string file, std::uint32_t line);
    IndexOutOfBoundsException(std::size_t index, std::string file, std::uint32_t line);
    IndexOutOfBoundsException(std::size_t index, std::size_t arrayLength, std::string file, std::uint32_t line);

private:
    std::size_t index;
    std::size_t arrayLength;

};

}
