#pragma once


#include "Exception.hpp"

namespace tge {

class WriteOnceException : public Exception {

public:
    std::string getVariableName() const;
    
    WriteOnceException(std::string variableName, std::string file, std::uint32_t line);

private:
    std::string variableName;

};

}