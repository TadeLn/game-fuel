#pragma once

#include "Exception.hpp"


namespace tge {

class PropertyNotFoundException : public Exception {

public:
    std::string getPropertyName() const;
    PropertyNotFoundException(std::string file, std::uint32_t line);
    PropertyNotFoundException(std::string propertyName, std::string file, std::uint32_t line);

private:
    std::string propertyName;

};

}
