#include "IndexOutOfBoundsException.hpp"


namespace tge {

std::size_t IndexOutOfBoundsException::getIndex() const {
    return index;
}

std::size_t IndexOutOfBoundsException::getArrayLength() const {
    return arrayLength;
}


IndexOutOfBoundsException::IndexOutOfBoundsException(std::string file, std::uint32_t line)
    : OutOfBoundsException("array index out of bounds", file, line) {}

IndexOutOfBoundsException::IndexOutOfBoundsException(std::size_t index, std::string file, std::uint32_t line)
    : OutOfBoundsException("array index out of bounds: " + std::to_string(index) + " >= array length", file, line) {
    this->index = index;
}

IndexOutOfBoundsException::IndexOutOfBoundsException(std::size_t index, std::size_t arrayLength, std::string file, std::uint32_t line)
    : OutOfBoundsException("array index out of bounds: " + std::to_string(index) + " >= " + std::to_string(arrayLength), file, line) {
    this->index = index;
    this->arrayLength = arrayLength;
}

}