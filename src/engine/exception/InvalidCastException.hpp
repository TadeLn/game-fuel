#pragma once

#include "Exception.hpp"


namespace tge {

class InvalidCastException : public Exception {

public:
    InvalidCastException(std::string file, std::uint32_t line);

};

}
