#include "Exception.hpp"



namespace tge {

std::string Exception::getFile() const {
    return file;
}

std::uint32_t Exception::getLine() const {
    return line;
}

std::string Exception::getMessage() const {
    return message;
}


void Exception::print(Logger& logger, const std::string& preMessage) {
    logger.error(preMessage + (preMessage.empty() ? "" : ": ") + "An exception occurred: " + message + " @ " + file + ":" + std::to_string(line));
}

void Exception::print(Logger& logger, std::string file, std::uint32_t line, const std::string& preMessage) {
    this->print(logger, preMessage);
    logger.info(std::string() + "(caught @ " + file + ":" + std::to_string(line) + " )");
}

const char* Exception::what() const noexcept {
    return this->message.c_str();
}



Exception::Exception(const std::exception& exception)
    : Exception (exception, EXCTX)
{}

Exception::Exception(const std::exception& exception, std::string file, std::uint32_t line)
    : Exception (exception.what(), file, line)
{}



Exception::Exception(std::string file, std::uint32_t line)
    : Exception(std::string(), file, line)
{}

Exception::Exception(std::string message, std::string file, std::uint32_t line) {
    this->file = file;
    this->line = line;
    this->message = message;
}



void Exception::_assert(bool check, std::string file, std::uint32_t line) {
    if (!check) {
        throw Exception("assertion failed", file, line);
    }
}

void Exception::_assert(bool check, Exception ex) {
    if (!check) {
        throw ex;
    }
}

};
