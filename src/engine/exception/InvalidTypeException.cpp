#include "InvalidTypeException.hpp"


namespace tge {

InvalidTypeException::InvalidTypeException(std::string file, std::uint32_t line)
    : Exception("invalid type", file, line) {}

}