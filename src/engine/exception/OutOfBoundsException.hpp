#pragma once

#include "Exception.hpp"


namespace tge {

class OutOfBoundsException : public Exception {

public:
    OutOfBoundsException(std::string file, std::uint32_t line);

protected:
    OutOfBoundsException(std::string message, std::string file, std::uint32_t line);

};

}
