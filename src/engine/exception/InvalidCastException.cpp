#include "InvalidCastException.hpp"


namespace tge {

InvalidCastException::InvalidCastException(std::string file, std::uint32_t line)
    : Exception("invalid cast to another type", file, line) {}

}