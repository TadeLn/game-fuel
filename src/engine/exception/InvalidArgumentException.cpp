#include "InvalidArgumentException.hpp"


namespace tge {

std::string InvalidArgumentException::getArgumentName() const {
    return argumentName;
}

std::string InvalidArgumentException::getDescription() const {
    return description;
}


InvalidArgumentException::InvalidArgumentException(std::string file, std::uint32_t line)
    : Exception("invalid value of argument", file, line) {}

InvalidArgumentException::InvalidArgumentException(std::string argumentName, std::string file, std::uint32_t line)
    : Exception("invalid value of argument \"" + argumentName + "\"", file, line) {
    this->argumentName = argumentName;
}

InvalidArgumentException::InvalidArgumentException(std::string argumentName, std::string description, std::string file, std::uint32_t line)
    : Exception("invalid value of argument \"" + argumentName + "\": " + description, file, line) {
    this->argumentName = argumentName;
    this->description = description;
}

}