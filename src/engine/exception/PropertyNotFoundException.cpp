#include "PropertyNotFoundException.hpp"


namespace tge {

std::string PropertyNotFoundException::getPropertyName() const {
    return propertyName;
}


PropertyNotFoundException::PropertyNotFoundException(std::string file, std::uint32_t line)
    : Exception("property not found", file, line) {}

PropertyNotFoundException::PropertyNotFoundException(std::string propertyName, std::string file, std::uint32_t line)
    : Exception("property \"" + propertyName + "\" not found", file, line) {
    this->propertyName = propertyName;
}

}