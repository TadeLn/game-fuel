#pragma once

#include <string>

#include "../log/Logger.hpp"


#ifndef EXCTX
#define EXCTX std::string(__FILE__), __LINE__
#endif


namespace tge {

class Exception : public std::exception {

public:
    // Get filename
    std::string getFile() const;

    // Get line number
    std::uint32_t getLine() const;

    // Get exception message
    std::string getMessage() const;


    // Print message to console
    void print(Logger& logger, const std::string& preMessage = "");

    // Print message to console
    void print(Logger& logger, std::string file, std::uint32_t line, const std::string& preMessage = "");

    const char* what() const noexcept;


    // Convert std::exception to Exception
    Exception(const std::exception& exception);

    // Convert std::exception to Exception
    Exception(const std::exception& exception, std::string file, std::uint32_t line);


    // Throw an exception if the statement is false
    static void _assert(bool check, std::string file, std::uint32_t line);

    // Throw an exception if the statement is false
    static void _assert(bool check, Exception ex);

protected:
    // Construct the exception
    Exception(std::string file, std::uint32_t line);

    // Construct the exception with a message
    Exception(std::string message, std::string file, std::uint32_t line);

    // Message of the exception
    std::string message;

private:
    // Location of the exception (file)
    std::string file;

    // Location of the exception (line)
    std::uint32_t line;

};

}
