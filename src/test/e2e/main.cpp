#include <iostream>

#include "engine/Application.hpp"
#include "engine/String.hpp"
#include "engine/Window.hpp"
#include "engine/event/KeyboardEvent.hpp"
#include "engine/input/Mouse.hpp"
#include "engine/scene/Scene.hpp"
#include "engine/scene/gameObject/CameraObject.hpp"
#include "engine/scene/gameObject/TileRendererObject.hpp"
#include "engine/scene/gameObject/SpriteObject.hpp"
#include "engine/log/Logger.hpp"
#include "engine/texture/TextureAtlas.hpp"
#include "engine/TilePalette.hpp"
#include "engine/exception/InvalidOperationException.hpp"

enum Tile
{
    TILE_WATER = 0,
    TILE_GRASS = 1,
    TILE_PATH = 2
};

class Game;

class CharacterObject : public tge::SpriteObject
{

private:
    Game *game;

public:
    void onCreate(__attribute__((unused)) tge::Scene *scene)
    {
        setSize(tge::Vec2d(tge::TileRendererObject::TILE_WIDTH, tge::TileRendererObject::TILE_HEIGHT));
    }

    void onFrame(tge::Scene *scene, tge::Window &win, double deltaTime);
    bool isSwimming();
    CharacterObject(Game *game);
};

class Game : public tge::Application
{

private:
    tge::Logger logger = tge::Logger("main");

    std::shared_ptr<tge::TextureAtlas> atlas;
    tge::TileMap2D<tge::TileID> tileMap;

    tge::UUID mainCameraUUID;
    tge::UUID hudCameraUUID;
    tge::UUID tileRendererUUID;
    tge::UUID playerSpriteUUID;

    bool debugMode = false;

    void setDebug(bool debug)
    {
        using namespace tge;

        this->debugMode = debug;

        auto tileRenderer = getObject<TileRendererObject>(tileRendererUUID);
        auto playerSprite = getObject<CharacterObject>(playerSpriteUUID);

        if (debug)
        {
            tileRenderer->debugMode = true;
        }
        else
        {
            tileRenderer->debugMode = false;
        }
    }

    void toggleDebug()
    {
        setDebug(!debugMode);
    }

    void onSignal(int signalNum)
    {
        logger.log(std::string("Recieved interrupt: ") + std::to_string(signalNum));
        this->stop();
    }

    void onKeyboard(const tge::KeyboardEvent &event)
    {
        using namespace tge;

        // logger.debug(String::str("onKeyboard: ", event));

        if (event.getKeyState() == KeyboardEvent::KeyState::PRESSED)
        {
            if (event.getKey() == Keyboard::Key::O)
            {
                logger.debug(String::str("Current objects on scene: ", this->activeScene->getObjects()));
            }
            else if (event.getKey() == Keyboard::Key::F3)
            {
                toggleDebug();
            }
        }
    }

    void onText(__attribute__((unused)) const tge::TextEvent &event)
    {
        // logger.debug(std::string("onText: ") + event.toString());
    }

    void onInit(const std::vector<std::string> &commandLineArguments, tge::Window &win)
    {
        using namespace tge;

        logger.info("\n"
                    "Hello world\n"
                    "\n"
                    "Command line arguments:\n");

        for (std::string argument : commandLineArguments)
        {
            logger.log(argument);
        }

        win.setFpsLimit(144);
        win.setVSync(false);

        this->activeScene = new Scene("Main Scene");

        tileMap = TileMap2D<TileID>(100, 50);
        for (std::uint32_t x = 0; x < tileMap.getWidth(); x++)
        {
            tileMap.trySetTile(x, x - 3, TILE_GRASS);
            tileMap.trySetTile(x, x - 2, TILE_GRASS);
            tileMap.trySetTile(x, x - 1, TILE_GRASS);
            tileMap.trySetTile(x, x + 0, TILE_PATH);
            tileMap.trySetTile(x, x + 1, TILE_PATH);
            tileMap.trySetTile(x, x + 2, TILE_GRASS);
            tileMap.trySetTile(x, x + 3, TILE_GRASS);
            tileMap.trySetTile(x, x + 4, TILE_GRASS);
        }

        atlas = std::make_shared<TextureAtlas>("texture.png");

        TilePalette tilePalette = TilePalette<TileID>(atlas);
        tilePalette.assign(TILE_WATER, "tile_water");
        tilePalette.assign(TILE_GRASS, "tile_grass");
        tilePalette.assign(TILE_PATH, "tile_path");

        this->mainCameraUUID = this->activeScene->addObject(std::make_unique<CameraObject>(800, 0, CameraObject::Direction::HORIZONTAL));
        this->hudCameraUUID = this->activeScene->addObject(std::make_unique<CameraObject>(800, 0, CameraObject::Direction::HORIZONTAL));
        this->tileRendererUUID = this->activeScene->addObject(std::make_unique<TileRendererObject>(tileMap, tilePalette));
        this->playerSpriteUUID = this->activeScene->addObject(std::make_unique<CharacterObject>(this));

        win.setActiveCamera(getObject<CameraObject>(this->mainCameraUUID));

        logger.info(std::string("Active scene: ") + this->activeScene->toString());
    }

    void onFrame(tge::Window &win, __attribute__((unused)) double deltaTime)
    {
        using namespace tge;

        win.setTitle("Game fps: " + std::to_string(this->getFps()));

        win.setActiveCamera(getObject<CameraObject>(this->hudCameraUUID));

        auto mappedCoords = win.getSfWindow().mapPixelToCoords((sf::Vector2i)Mouse::getPosition(win));
        win.drawRectangleTest(mappedCoords.x, mappedCoords.y, 20, 20, Mouse::isButtonPressed(Mouse::Button::LEFT) ? Color4::CYAN : Color4::RED);

        std::shared_ptr<CameraObject> mainCamera = getObject<CameraObject>(mainCameraUUID);
        std::shared_ptr<CharacterObject> playerSprite = getObject<CharacterObject>(playerSpriteUUID);
        mainCamera->setPosition(playerSprite->getPosition());

        win.setActiveCamera(mainCamera);

        // logger.debug("frame, delta time: " + std::to_string(deltaTime));
    }

public:
    Game() : tge::Application("Game Engine Test") {}

    const std::shared_ptr<const tge::TextureAtlas> getAtlas()
    {
        return this->atlas;
    }
    tge::Logger getLogger()
    {
        return this->logger;
    }
    const tge::TileMap2D<tge::TileID> &getTileMap()
    {
        return this->tileMap;
    }
};

void CharacterObject::onFrame(tge::Scene *scene, tge::Window &win, double deltaTime)
{
    using namespace tge;
    SpriteObject::onFrame(scene, win, deltaTime);

    bool swimming = isSwimming();

    bool moved = false;
    const float moveSpeed = swimming ? 0.6 : 1.0;
    if (Keyboard::isPressed(Keyboard::Key::ARROW_LEFT))
    {
        moved = true;
        this->move(Vec2d(-moveSpeed, 0));
    }
    if (Keyboard::isPressed(Keyboard::Key::ARROW_RIGHT))
    {
        moved = true;
        this->move(Vec2d(+moveSpeed, 0));
    }
    if (Keyboard::isPressed(Keyboard::Key::ARROW_UP))
    {
        moved = true;
        this->move(Vec2d(0, -moveSpeed));
    }
    if (Keyboard::isPressed(Keyboard::Key::ARROW_DOWN))
    {
        moved = true;
        this->move(Vec2d(0, +moveSpeed));
    }

    if (moved)
    {
        std::uint32_t frame = (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count() % 250) / 125;

        this->setTextureFragment(game->getAtlas()->getFragment(swimming ? "sprite_player_swim" : "sprite_player", frame));
    }
}

bool CharacterObject::isSwimming()
{
    game->getLogger().log(this->getRect().toString());
    return this->game->getTileMap().getTileOrOutOfBounds(
               this->getRect().getCenter().x / tge::TileRendererObject::TILE_WIDTH,
               (this->getPosition().y + this->getSize().y) / tge::TileRendererObject::TILE_HEIGHT) == TILE_WATER;
}

CharacterObject::CharacterObject(Game *game) : tge::SpriteObject(game->getAtlas()->getFragment("sprite_player", 0))
{
    this->game = game;
}

int main(int argc, char **argv)
{
    Game game;
    return game.start(argc, argv);
}
